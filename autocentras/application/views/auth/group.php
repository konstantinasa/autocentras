<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('see_group_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-default" href="<?php echo base_url('auth/create_group'); ?>"><i class="fa fa-pencil fa-fw"></i><?php echo lang('index_create_group_link') ?></a>
            </br>
            </br>
        </div>
        <!-- /.col-lg-12-->
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th><?php echo lang('see_group_th'); ?></th>
                                <th><?php echo lang('see_group_desc_th'); ?></th>
                                <th colspan="2"><center><?php echo lang('see_group_action_th'); ?></center></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($groups as $group): ?>
                            <tr>
                                <td>
                                        <?php echo($group->name); ?><br/>
                                </td>
                                <td>
                                    <?php echo($group->description); ?><br/>
                                </td>
                                <td><?php echo anchor("auth/edit_group/" . $group->id, lang('see_group_edit')); ?></td>
                                <td><?php echo anchor("auth/delete_group/" . $group->id, lang('see_group_delete')); ?></td>
                            </tr>
                            </tbody>
                            <?php endforeach; ?>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->