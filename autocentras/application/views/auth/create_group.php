<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_group_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="help-block"><?php echo lang('create_group_subheading'); ?></p>
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("auth/create_group"); ?>
                            <div class="form-group">
                                <label><?php echo lang('create_group_name_label', 'group_name'); ?></label>
                                <?php echo form_input($group_name); ?>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('create_group_desc_label', 'description'); ?></label>
                                <?php echo form_input($description); ?>
                            </div>
                            <button type="submit" class="btn btn-primary">
                                <?php echo lang('create_group_submit_btn'); ?>
                            </button>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
