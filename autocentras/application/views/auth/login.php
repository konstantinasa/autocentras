<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo lang('login_heading'); ?></h3>
                </div>
                <div class="panel-body">
                    <?php echo validation_errors(); ?>
                    <?php echo form_open("auth/login"); ?>
                    <fieldset>
                        <div class="form-group">
                            <?php echo form_input($identity); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_input($password); ?>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-lg btn-success btn-block" type="submit"
                                    name="sumbit"><?php echo lang('login_submit_btn'); ?></button>
                                 <span class="pull-right">
                            <p>&nbsp;</p>
                                                   </div>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>