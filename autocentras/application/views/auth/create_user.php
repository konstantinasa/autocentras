<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_user_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="help-block"><?php echo lang('create_user_subheading'); ?></p>
                            <?php echo validation_errors(); ?>
                            <div id="infoMessage"><?php echo $message;?></div>
                            <?php echo form_open("auth/create_user"); ?>
                            <div class="form-group">
                                <label><?php echo lang('create_user_fname_label', 'first_name'); ?></label>
                                <?php echo form_input($first_name); ?>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('create_user_lname_label', 'last_name'); ?></label>
                                <?php echo form_input($last_name); ?>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('create_user_email_label', 'email'); ?></label>
                                <?php echo form_input($email); ?>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('create_user_password_label', 'password'); ?></label>
                                <?php echo form_input($password); ?>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('create_user_password_confirm_label', 'password_confirm'); ?></label>
                                <?php echo form_input($password_confirm); ?>
                            </div>
                            
                            <!-------------------------------------------------  -->
                            <div class="form-group">
                                <?php if ($this->ion_auth->is_admin()): ?>
                                    <label> <?php echo lang('edit_user_groups_heading'); ?></label>
                                    <?php foreach ($groups as $group): ?>
                                        <div class="checkbox">
                                            <label>
                                                <?php
                                                $gID = $group['id'];
                                                $checked = null;
                                                $item = null;
                                                ?>
                                                <input type="radio" name="groups[]"
                                                       value="<?php echo $group['id']; ?>"<?php echo $checked; ?>>
                                                <?php echo($group['name']); ?>
                                            </label>
                                        </div>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </div>
                            
                            <!-------------------------------------------------------->
                            
                            <button type="submit" class="btn btn-primary">
                                <?php echo lang('create_user_submit_btn'); ?>
                            </button>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
