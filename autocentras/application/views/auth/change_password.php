<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('change_password_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo validation_errors(); ?>
                            <?php echo form_open("auth/change_password"); ?>
                            <div class="form-group">
                                <label> <?php echo lang('change_password_old_password_label', 'old_password'); ?> </label>
                                <?php echo form_input($old_password); ?>
                            </div>
                            <div class="form-group">
                                <label
                                    for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length); ?></label>
                                <?php echo form_input($new_password); ?>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm'); ?> </label>
                                <?php echo form_input($new_password_confirm); ?>
                            </div>
                            <?php echo form_input($user_id); ?>
                            <button type="submit" class="btn btn-primary">
                                <?php echo lang('change_password_submit_btn'); ?>
                            </button>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
