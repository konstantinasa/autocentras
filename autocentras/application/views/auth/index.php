<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('index_heading'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-default" href="<?php echo base_url('auth/create_user'); ?>"><i class="fa fa-pencil fa-fw"></i><?php echo lang('index_create_user_link') ?></a>
            </br>
            </br>
        </div>
        <!-- /.col-lg-12-->
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo lang('index_subheading'); ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th><?php echo lang('index_fname_th'); ?></th>
                                <th><?php echo lang('index_lname_th'); ?></th>
                                <th><?php echo lang('index_username_th'); ?></th>
                                <th><?php echo lang('index_email_th'); ?></th>
                                <th><?php echo lang('index_groups_th'); ?></th>
                                <th><?php echo lang('index_status_th'); ?></th>
                                <th colspan="2"><center><?php echo lang('index_action_th'); ?></center></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($users as $user): ?>
                            <tr>
                                <td><?php echo($user->first_name); ?></td>
                                <td><?php echo($user->last_name); ?></td>
                                <td><?php echo($user->username); ?></td>
                                <td><?php echo($user->email); ?></td>
                                <td>
                                    <?php foreach ($user->groups as $group): ?>
                                       <?php echo ($group->name); ?><br/>
                                    <?php endforeach ?>
                                </td>
                                <td><?php echo ($user->active) ? anchor("auth/deactivate/" . $user->id, lang('index_active_link')) : anchor("auth/activate/" . $user->id, lang('index_inactive_link')); ?></td>
                                <td><?php echo anchor("auth/edit_user/" . $user->id, lang('index_edit')); ?></td>
                                <td><?php echo anchor("auth/delete_user/" . $user->id, lang('index_delete'), array('onclick' => "return confirm('Ar tikrai norite ištrinti?')")); ?></td>
                            </tr>
                            </tbody>
                            <?php endforeach; ?>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12-->
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->