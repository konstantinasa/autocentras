<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url(); ?>general/main/home">Autocentras</a>
    </div>
    <!-- /.navbar-header -->


    <ul class="nav navbar-top-links navbar-right">
        <li><strong><?php echo "Sveiki atvyke" ?></strong></li>
        <li class="dropdown">
<!--            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>-->
            <!--<ul class="dropdown-menu dropdown-user">-->
                <li><a href="<?php echo base_url(); ?>auth/create_user"><i class="fa fa-gear fa-fw"></i> <?php echo lang("register") ?></a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url(); ?>auth/login"><i class="fa fa-sign-out fa-fw"></i><?php echo lang('sign_in')?></a></li>
                 <!--<a href="<?php echo base_url(); ?>auth/logout">ll</a>-->
            <!--</ul>-->
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
