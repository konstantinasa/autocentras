<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            
            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('main_page') ?></a></li>            
            
            <li><a href="<?php echo base_url(); ?>auth/index"><i class="fa fa-wrench fa-fw"></i> <?php echo lang("users_management") ?><spanclass="fa arrow"></span></a></li>
            
            <li><a href="<?php echo base_url(); ?>general/orders/seeOrders"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('all_orders') ?></a></li>
            
            <li><a href="<?php echo base_url(); ?>general/messages/createMessage"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('leave_message') ?></a></li>
            
            <li><a href="<?php echo base_url(); ?>general/messages/displayMessages"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('all_messages') ?></a></li>
                        
            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('contacts') ?></a></li>
            
            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('about_us') ?></a></li>

            
            <li>
               <a href="#"><i class="fa fa-edit fa-fw"></i><?php echo lang('add_info') ?><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                   <li><a href="<?php echo base_url(); ?>general/services/seeServices"><i class="fa fa-pencil fa-fw"></i><?php echo lang('services_list') ?></a></li>
                   <!--<li><a href="<?php echo base_url(); ?>general/times/seeTimes"><i class="fa fa-pencil fa-fw"></i><?php echo lang('times_list') ?></a></li>-->
                   <li><a href="<?php echo base_url(); ?>general/template"><i class="fa fa-pencil fa-fw"></i><?php echo lang('template') ?></a></li>
                   

                    <!--<li><a href="<?php echo base_url(); ?>#"><i class="fa fa-pencil fa-fw"></i>sss</a></li>-->
                </ul> 
            </li>
            
            
        </ul>
             
            
           
<!--            <li>
                <a href="blank.html">Blank Page</a>
            </li>-->
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>