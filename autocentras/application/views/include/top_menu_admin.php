<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url(); ?>general/main/home"><?php echo lang('header') ?></a>
    </div>
    <!-- /.navbar-header -->


    <ul class="nav navbar-top-links navbar-right">
        <li><strong><?php echo sprintf(lang('include_welcome'), $username); ?></strong></li>
        <li class="dropdown">
<!--            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>-->
            <!--<ul class="dropdown-menu dropdown-user">-->
                <li><a href="<?php echo base_url(); ?>auth/change_password"><i class="fa fa-gear fa-fw"></i> <?php echo lang("include_change_password") ?></a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url(); ?>auth/logout"><i class="fa fa-sign-out fa-fw"></i><?php echo lang('index_logout')?></a></li>
                 <!--<a href="<?php echo base_url(); ?>auth/logout">ll</a>-->
            <!--</ul>-->
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
