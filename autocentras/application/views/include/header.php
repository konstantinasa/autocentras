<!DOCTYPE html>
<html lang="lt">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo lang('header') ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
   
    <!--Data Time Picker-->
    <link href="<?php echo base_url('dtm/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet" media="screen">

    <!--DropDown CheckBox with Search field-->
    <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('drp_chbox/dependencies/css/bootstrap.css'); ?>" />-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('drp_chbox/bootstrap-dropdown-checkbox.css'); ?>" />
   <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url('drp_chbox/main.css'); ?>" />-->
    
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url('assets/bower_components/metisMenu/dist/metisMenu.min.css'); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/dist/css/sb-admin-2.css'); ?>" rel="stylesheet">
   
    <link href="<?php echo base_url('assets/css/bar_style.css'); ?>" rel="stylesheet">
    
    <link href="<?php echo base_url('assets/dist/css/select2.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/dist/css/select2.min.css'); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>"
          rel="stylesheet" type="text/css">

    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
  
    
</head>
<body onload="checkBoxDis()">
<div id="wrapper">