<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            
            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('main_page') ?></a></li>
          
            <li><a href="<?php echo base_url(); ?>general/orders/createNewOrder"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('service_order') ?></a></li>
     
            <li><a href="<?php echo base_url(); ?>general/messages/createMessage"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('leave_message') ?></a></li>

            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('see_orders') ?></a></li>
            
            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('about_us') ?></a></li>
            
            <li><a href="<?php echo base_url(); ?>general/main/home"><i class="fa fa-dashboard fa-fw"></i> <?php echo lang('contacts') ?></a></li>

           
<!--            <li>
                <a href="blank.html">Blank Page</a>
            </li>-->
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>