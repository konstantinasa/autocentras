
</div>
<!-- /#wrapper -->


<!-- script references -->
<script src="<?php echo base_url('assets/javascript/jquery-2.1.3.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/bootstrap-filestyle.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js'); ?>"></script>

<script src="<?php echo base_url('assets/javascript/script.js') ?>"></script>




<!-- jQuery -->
<!--<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script>-->

<!-- Bootstrap Core JavaScript -->
<!--<script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>-->

<!--Date Time Picker JavaScript-->
<script type="text/javascript" src="<?php echo base_url('dtm/js/bootstrap-datetimepicker.js'); ?>" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url('dtm/js/locales/bootstrap-datetimepicker.lt.js'); ?>" charset="UTF-8"></script>

<!--dropdown checkbox with search field-->
<script src="<?php echo base_url('drp_chbox/bootstrap-dropdown-checkbox.js'); ?>"></script>


<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url('assets/bower_components/metisMenu/dist/metisMenu.min.js') ?>"></script>


<!-- Morris Charts JavaScript -->
<!-- <script src="<?php echo base_url('assets/bower_components/raphael/raphael-min.js') ?>"></script> -->
<!-- <script src="<?php echo base_url('assets/bower_components/morrisjs/morris.min.js') ?>"></script> -->
<!-- <script src="<?php echo base_url('assets/js/morris-data.js') ?>"></script> -->


<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url('assets/dist/js/sb-admin-2.js') ?>"></script>

<script src="<?php echo base_url('assets/dist/js/select2.full.js') ?>"></script>
<script src="<?php echo base_url('assets/dist/js/select2.full.min.js') ?>"></script>

<!--<script src="<?php echo base_url('assets/dist/js/select2.js') ?>"></script>
<script src="<?php echo base_url('assets/dist/js/underscore.js') ?>"></script>-->


<!--//----------------------->
<script type="text/javascript" src="<?php echo base_url('assets/js_k/script_k.js'); ?>" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js_k/check_box_dis.js'); ?>" charset="UTF-8"></script>

<!------- script dropdown checkbox with search field-->
<script>  

// Ligos 2k
var ligosArrayFromPhp = <?php echo json_encode($resultLigos); ?>;
    var ligos = [];
    
$.each(ligosArrayFromPhp, function (i, elem) {
    ligos.push({
        id: elem.id,
        label: elem.pavadinimas,
        isChecked : elem.isChecked
    });
});

//  Masitas  
    var arrayFromPHP = <?php echo json_encode($resultFood); ?>;
    var food = [];
    
$.each(arrayFromPHP, function (i, elem) {
    food.push({
        id: elem.id,
        label: elem.pasirinkimas,
        isChecked : elem.isChecked
    });
});


// Alergenai 
var alergArrayFromPhp = <?php echo json_encode($resultAllergy); ?>;
    var alerg = [];
    
$.each(alergArrayFromPhp, function (i, elem) {
    alerg.push({
        id: elem.id,
        label: elem.alergiskaskam,
        isChecked : elem.isChecked
    });
});

// Onkologines
var onkolArrayFromPhp = <?php echo json_encode($resultSrgVez); ?>;
    var srg_vez = [];
    
$.each(onkolArrayFromPhp, function (i, elem) {
    srg_vez.push({
        id: elem.id,
        label: elem.aprasas,
        isChecked : elem.isChecked
    });
});



  var ligos_field;  
  var food_field;

  var alerg_field;
  var srg_vez_field;
  

  $('.ligos_dropdown_checkbox').dropdownCheckbox({
    data: ligos,
    autosearch: true,
    title: "Ligos",
    hideHeader: false,
    showNbSelected: true,
    maxItems: 500,
    templateButton: '<a class="dropdown-checkbox-toggle btn btn-outline btn-primary col-md-5" data-toggle="dropdown" width="48" href="#">Ligos <span class="dropdown-checkbox-nbselected"></span><b class="caret"></b></a>'
  });
    
    
    
    $('.food_dropdown_checkbox').dropdownCheckbox({
    data: food,

    autosearch: true,
    title: "Maisto pasirinkimai",
    hideHeader: false,
    showNbSelected: true,
    maxItems: 500,
    templateButton: '<a class="dropdown-checkbox-toggle btn btn-outline btn-primary col-md-5" data-toggle="dropdown" width="48" href="#">Maisto pasirinkimai <span class="dropdown-checkbox-nbselected"></span><b class="caret"></b></a>'
  });
  

  
  $('.alerg_dropdown_checkbox').dropdownCheckbox({
    data: alerg,
    autosearch: true,
    title: "Alergenai",
    hideHeader: false,
    showNbSelected: true,
    maxItems: 500,
    templateButton: '<a class="dropdown-checkbox-toggle btn btn-outline btn-primary col-md-5" data-toggle="dropdown" width="48" href="#">Alergenai <span class="dropdown-checkbox-nbselected"></span><b class="caret"></b></a>'
  });
  
  $('.srg_vez_dropdown_checkbox').dropdownCheckbox({
    data: srg_vez,
    autosearch: true,
    title: "Alergenai",
    hideHeader: false,
    showNbSelected: true,
    maxItems: 500,
    templateButton: '<a class="dropdown-checkbox-toggle btn btn-outline btn-primary col-md-5" data-toggle="dropdown" width="48" href="#">Onkologinės ligos <span class="dropdown-checkbox-nbselected"></span><b class="caret"></b></a>'
  });
   
    
    function getArray(field){
        var tmp = field.checked();
        var checked = "";
        $.each(tmp, function (i, elem) {
            checked += tmp[i].id + ",";
    });
    return checked;
    }
  
   ligos_field = $('.ligos_dropdown_checkbox').data('dropdownCheckbox');
  food_field = $('.food_dropdown_checkbox').data('dropdownCheckbox');
  alerg_field = $('.alerg_dropdown_checkbox').data('dropdownCheckbox');
  srg_vez_field = $('.srg_vez_dropdown_checkbox').data('dropdownCheckbox');
 

  
  function send_arr(){
       var checked_arr_ligos = getArray(ligos_field);
      document.getElementById("ligos").value = checked_arr_ligos;
      
      var checked_arr_food = getArray(food_field);
      document.getElementById("food").value = checked_arr_food;
      
      
      var checked_arr_alerg = getArray(alerg_field);
      document.getElementById("alerg").value = checked_arr_alerg;

      var checked_arr_srg_vez = getArray(srg_vez_field);
      document.getElementById("srg_vez").value = checked_arr_srg_vez;
      
      }   
      
 
   function list(size, checked) {
    var result = [];
    for (var i = 0; i < size; i++) {
      result.push({
        id: i,
        label: 'Item #' + i,
        isChecked: checked === undefined ? !!(Math.round(Math.random() * 1)) : checked
      });
    }
    return result;
  }
 
</script>


</body>
</html>