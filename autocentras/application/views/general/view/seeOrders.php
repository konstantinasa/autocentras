<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> <?php echo lang('all_orders') ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
<!--            <div class="panel panel-default">
                <div class="panel-body">-->
                    <div class="row">
                        <div class="col-lg-12">
        
        <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo lang('all_orders') ?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                <div class="contentCreate">
                        <?php echo anchor("general/orders/createNewOrder", lang('create_order'),
                                array(
                                        'id' => 'createOrder',
                                        'name' => 'createOrder',
                                        'class' => 'btn btn-success',
                                        'type' => 'button'
                                    ));
                        
//                      
                      
                        echo br(2);
                        ?>
                </div><!--contentCreate-->
                                
 <p><center><?php echo $links; ?></center></p>                                
									
<div id="exTab2" class="container">

<!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active">
          <a href="#info_1" role="tab" data-toggle="tab" title="">
               <?php echo lang('order_info_1') ?> <icon class="fa fa-user"></icon>
          </a>
      </li>
      <li><a href="#info_2" role="tab" data-toggle="tab" title="kazkas &#13 tas tas &#13 ">
           <?php echo lang('order_info_2') ?> <i class="fa fa-home"></i>
          </a>
      </li>
      <li>
          <a href="#info_3" role="tab" data-toggle="tab" title="">
              <?php echo lang('order_info_3') ?>
          </a>
      </li>
    </ul>

<!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane fade active in" id="info_1">
                    <div class="dataTable_wrapper">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
				<thead>
					<tr>
                                                <td class="tableBoldGreen"><?php echo lang('order_id') ?></td>
						<td class="tableBoldGreen"><?php echo lang('service_name') ?></td>
                                                <td class="tableBoldGreen"><?php echo lang('notes') ?></td>	
                                                <td class="tableBoldGreen"><?php echo lang('arrival_time') ?></td>
                                                <td class="tableBoldGreen"><?php echo lang('arrival_date') ?></td>
                                                <td class="tableBoldGreen"><?php echo lang('action_edit') ?></td>
                                                <td class="tableBoldGreen"><?php echo lang('action_delete') ?></td>
                                                
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllOrders) {
					foreach ($resultAllOrders as $row) {
						echo "<tr>";
						echo "<td>" . $row->id . "</td>";
						echo "<td>" . $row->service . "</td>";
                                                echo $row->service_descr_cust == '' ?  "<td> N/A</td>" : "<td>" . $row->service_descr_cust . "</td>";
                                                echo "<td>" . $row->laikas . "</td>";
						echo $row->atvykimo_data == '' ?  "<td> N/A </td>" : "<td>" . $row->atvykimo_data . "</td>";
                                                echo "<td>" . anchor("general/orders/edit_order/$row->id", "Redaguoti", array('class' => 'btn btn-outline btn-success','onclick' => "return confirm('Ar tikrai norite redaguoti?')")) . "</td>";
                                                echo "<td>" . anchor("general/orders/delete_order/$row->id", "Trinti", array('class' => 'btn btn-outline btn-danger','onclick' => "return confirm('Ar tikrai norite ištrinti?')")) . "</td>";
                                                
                                                
						echo "</tr>";
					}
				} else {
					?>
					<div class="infoMessage">
						<?php
						echo lang('no_information')
						?>
					</div>
					<!--infoMessage-->
					<?php
				}
				?>
				</tbody>
			</table>
                    </div>
		</div>
        
		 <div class="tab-pane fade" id="info_2">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
                            <div class="dataTable_wrapper">
                                <thead>
					<tr>
						
                                            <td class="tableBoldGreen"><?php echo lang('car_name') ?></td>
                                            <td class="tableBoldGreen"><?php echo lang('car_model') ?></td>
                                            <td class="tableBoldGreen"><?php echo lang('car_no') ?></td>	
                                            <td class="tableBoldGreen"><?php echo lang('car_insurance') ?></td>
		
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllOrders) {
					foreach ($resultAllOrders as $row) {
						echo "<tr>";
                                                echo $row->automobilis == '' ?  "<td> N/A </td>" : "<td>" . $row->automobilis . "</td>";
                                                echo $row->marke == '' ?  "<td> N/A </td>" : "<td>" . $row->marke . "</td>";
                                                echo $row->valst_nr == '' ?  "<td> N/A </td>" : "<td>" . $row->valst_nr . "</td>";
                                                echo $row->draudimas == '' ?  "<td> N/A </td>" : "<td>" . $row->draudimas . "</td>";
						echo "</tr>";
					}
				} else {
					?>
					<div class="infoMessage">
						<?php
						echo lang('no_information')
						?>
					</div>
					<!--infoMessage-->
					<?php
				}
				?>
				</tbody>
                           </div>
			</table>
		</div>
		 <div class="tab-pane fade" id="info_3">
			<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
                            <div class="dataTable_wrapper">
                                <thead>
					<tr>
                                            <td class="tableBoldGreen"><?php echo lang('fname') ?></td>
                                            <td class="tableBoldGreen"><?php echo lang('lname') ?></td>
                                            <td class="tableBoldGreen"><?php echo lang('tel_no') ?></td>	
                                            <td class="tableBoldGreen"><?php echo lang('email') ?></td>   
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllOrders) {
					foreach ($resultAllOrders as $row) {
                                            echo "<tr>";
						echo $row->vardas == '' ?  "<td> N/A </td>" : "<td>" . $row->vardas . "</td>";
                                                echo $row->pavarde == '' ?  "<td> N/A </td>" : "<td>" . $row->pavarde . "</td>";
                                                echo $row->tel_nr == '' ?  "<td> N/A </td>" : "<td>" . $row->tel_nr . "</td>";
                                                echo $row->el_pastas == '' ?  "<td> N/A </td>" : "<td>" . $row->el_pastas . "</td>";
                                            echo "</tr>";
					}
				} else {
					?>
					<div class="infoMessage">
						<?php
						echo lang('no_information')
						?>
					</div>
					<!--infoMessage-->
					<?php
				}
				?>
				</tbody>
                            </div>
			</table>
		</div>
</div>
        <p><center><?php echo $links; ?></center></p>
        
        
<!--        <p><?php // echo var_dump($this->session->all_userdata()); ?></p>
         <p><?php // echo var_dump($this->session->userdata('username')); ?></p>-->
         <!--<p><?php // echo var_dump($resultAllPatient); ?></p>-->
         <!--<p><?php // echo var_dump($resultAllPatient['0']->padid_chol_kiekis); ?></p>-->
         <!------------------------------------------------------>
         


         
         <!------- 2nd------------------------------------->
         
         
         
         
         <!--------------------------------------------------->
   </div>
</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
