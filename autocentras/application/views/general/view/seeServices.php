<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> <?php echo lang('all_services') ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
<!--            <div class="panel panel-default">
                <div class="panel-body">-->
                    <div class="row">
                        <div class="col-lg-12">
        
        <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo lang('all_services') ?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="contentCreate">
                        <?php echo anchor("general/services/createService", lang('create_service'),
                                array(
                                        'id' => 'createOrder',
                                        'name' => 'createOrder',
                                        'class' => 'btn btn-success',
                                        'type' => 'button'
                                    ));
                                echo br(2);
                                ?>
                                </div><!--contentCreate-->
                                
 <p><center><?php echo $links; ?></center></p>                                
									
<div id="exTab2" class="container">


			<table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="max-width: 90%;">
				<thead>
					<tr>
                                                <td class="tableBoldGreen"><?php echo lang('service_id') ?></td>
						<td class="tableBoldGreen"><?php echo lang('service') ?></td>
                                                <td class="tableBoldGreen"><?php echo lang('price_from') ?></td>	
                                                <td class="tableBoldGreen"><?php echo lang('price_to') ?></td>
                                                <td class="tableBoldGreen"><?php echo lang('description') ?></td>
                                                <td class="tableBoldGreen"><?php echo lang('action_edit') ?></td>
                                                <td class="tableBoldGreen"><?php echo lang('action_delete') ?></td>
					</tr>
				</thead>
				<tbody>
				<?php
				if ($resultAllServices) {
					foreach ($resultAllServices as $row) {
						echo "<tr>";
						echo "<td>" . $row->id . "</td>";
						echo "<td>" . $row->service . "</td>";
                                                echo "<td>" . $row->price_from . "</td>";
                                                echo "<td>" . $row->price_to . "</td>";
                                                echo "<td>" . $row->description . "</td>";
                                                echo "<td>" . anchor("general/services/edit_service/$row->id", "Redaguoti", array('class' => 'btn btn-outline btn-success')) . "</td>";                                               
                                                echo "<td>" . anchor("general/services/delete_service/$row->id", "Trinti", array('class' => 'btn btn-outline btn-danger','onclick' => "return confirm('Ar tikrai norite ištrinti?')")) . "</td>";
						echo "</tr>";
					}
				} else {
					?>
					<div class="infoService">
						<?php
						echo lang('no_information')
						?>
					</div>
					<!--infoService-->
					<?php
				}
				?>
				</tbody>
                            </table>
                            </div>

                            </div>
                            </div>
        <p><center><?php echo $links; ?></center></p>
        
        
<!--        <p><?php // echo var_dump($this->session->all_userdata()); ?></p>
         <p><?php // echo var_dump($this->session->userdata('username')); ?></p>-->
         <!--<p><?php // echo var_dump($resultAllPatient); ?></p>-->
         <!--<p><?php // echo var_dump($resultAllPatient['0']->padid_chol_kiekis); ?></p>-->
         <!------------------------------------------------------>
         


         
         <!------- 2nd------------------------------------->
         
         
         
         
         <!--------------------------------------------------->
   </div>
</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
