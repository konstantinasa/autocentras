<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('edit_order'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

        <?php
        
//        echo var_dump($resultsOrders);
        
        
        foreach ($resultsOrders as $row) {
            echo validation_errors();
            
            
            
            echo form_fieldset('Užsakymo ID: ' . $row->id);
            echo form_open('/general/orders/edit_order/' . $row->id);
   
            
            /* Paslauga  */
        echo form_label(lang("service") , 'paslauga');
        $dataServices = array(
            '0' => '-'
        );
        foreach ($resultServices as $row2) {
            $dataServices[$row2->id] = $row2->service;
        }
        echo form_dropdown('paslauga', $dataServices, $row->service , 'id="dropDownService"', 'class="form-control"');
        echo br(2);
        
        //-------------------- 1 step
        
        /* Aprasymas */
        echo form_label(lang("description"), 'aprasymas');
        $dataAprasymas = array(
            'name' => 'aprasymas',
            'id' => 'aprasymas',
            'size' => '500',
            'value' => $row->service_descr_cust,
            'class' => 'form-control',
            'placeholder' => 'Pastabos'
        );
        echo form_input($dataAprasymas);
        echo br(1);
        
        
        /* Atvykimo data */
        echo form_label(lang("arrival_date"), 'tdata');
        ?>
        
        <div class="input-group date form_date col-md-5" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
            <input name="tdata" id="tdata" class="form-control" size="16" type="text" value="<?php echo $row->atvykimo_data ?>" readonly>
            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
       <input type="hidden" id="dtp_input2" value="" />
            
        <?php
        echo br(1);
        
        
        /* Atvykimo laikas  */
        echo form_label(lang("arrival_time") , 'atvyk_laikas');
        $dataNationality = array(
            '0' => '-'
        );
        foreach ($resultTime as $row3) {
            $dataTime[$row3->id] = $row3->laikas;
        }
        echo form_dropdown('atvyk_laikas', $dataTime, $row->atvykimo_laikas, 'class="form-control"');
        echo br(2);
        
        
        //-------------------- 2 step
            
             /* Automobilis */
        echo form_label(lang("car"), 'car');
        $dataCar = array(
            'name' => 'car',
            'id' => 'car',
            'size' => '100',
            'value' => $row->automobilis,
            'class' => 'form-control',
            'placeholder' => 'pvz: Audi'
        );
        echo form_input($dataCar);
        echo br(1);
        
        /* Modelis */
        echo form_label(lang('model'), 'model');
        $dataModel = array(
            'name' => 'model',
            'id' => 'model',
            'size' => '100',
            'value' => $row->marke,
            'class' => 'form-control',
            'placeholder' => 'pvz: A8'
        );
        echo form_input($dataModel);
        echo br(1);
        
         /* Valst Nr*/
        echo form_label(lang('valst_no'), 'valst_nr');
        $dataValstNr = array(
            'name' => 'valst_nr',
            'id' => 'valst_nr',
            'size' => '100',
            'value' => $row->valst_nr,
            'class' => 'form-control',
            'placeholder' => 'ABC 123'
        );
        echo form_input($dataValstNr);
        echo br(1);
        
        /*Pagaminimo data*/
//        echo form_label(lang("production_date"), 'pdata');
        ?>
        
<!--        <div class="input-group date form_date col-md-5" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
            <input name="production_date" id="production_date" class="form-control" size="16" type="text" value="//<?php echo $row->manf_date; ?>" readonly>
            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
       <input type="hidden" id="dtp_input2" value="" />-->
            
        <?php
//        echo br(1);
        
        /* Draudimas*/
        echo form_label(lang('insurance'), 'insurance');
        $dataInsurance = array(
            'name' => 'insurance',
            'id' => 'insurance',
            'size' => '100',
            'value' => $row->draudimas,
            'class' => 'form-control',
            'placeholder' => 'pvz: Kasko'
        );
        echo form_input($dataInsurance);
        echo br(3);
        
        
        
        // ---------- 3 step
        
         /* Vardas */
        echo form_label(lang("fname"), 'fname');
        $dataName = array(
            'name' => 'fname',
            'id' => 'fname',
            'size' => '100',
            'value' => $row->vardas,
            'class' => 'form-control',
            'placeholder' => ''
        );
        echo form_input($dataName);
        echo br(1);
        
        /* Pavarde */
        echo form_label(lang('lname'), 'lname');
        $dataLName = array(
            'name' => 'lname',
            'id' => 'lname',
            'size' => '100',
            'value' => $row->pavarde,
            'class' => 'form-control',
            'placeholder' => ''
        );
        echo form_input($dataLName);
        echo br(1);
        
         /* Tel nr*/
        echo form_label(lang('tel_no'), 'tel_no');
        $dataValstNr = array(
            'name' => 'tel_no',
            'id' => 'tel_no',
            'size' => '100',
            'value' => $row->tel_nr,
            'class' => 'form-control',
            'placeholder' => ''
        );
        echo form_input($dataValstNr);
        echo br(1);
        
        /* El Pastas*/
        echo form_label(lang('email'), 'email');
        $dataEmail = array(
            'name' => 'email',
            'id' => 'email',
            'size' => '100',
            'value' => $row->el_pastas,
            'class' => 'form-control',
            'placeholder' => ''
        );
        echo form_input($dataEmail);
        echo br(3);
        
        
            
            
            
            echo form_fieldset_close();
            echo br(1);
            $buttonSubmit = array(
                'name' => 'mysubmit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Išsaugoti'
            );
            echo form_submit($buttonSubmit);
            ?>
                <?php echo anchor("general/orders/seeOrders", "Grįžti atgal",
                    array(  'name' => 'backSeeOrders',
                            'id' => 'backSeeOrders',
                            'class'=> 'btn btn-default'))
                ?>
            <?php
            echo form_close();
        }
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
