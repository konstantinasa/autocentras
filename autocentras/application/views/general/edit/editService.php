<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('edit_service'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

        <?php
        
//        echo var_dump($resultsServices);
        
        
        foreach ($resultServices as $row) {
            echo validation_errors();
            
            
            
            echo form_fieldset('Paslaugos ID: ' . $row->id);
            echo form_open('/general/services/edit_service/' . $row->id);
   
            echo set_value('service_name');
            
/* Paslauga */
        echo form_label(lang("service"), 'service');
        $dataService = array(
            'name' => 'service',
            'id' => 'service',
            'size' => '500',
            'value' => $row->service,
            'class' => 'form-control',
            'placeholder' => lang("service_name"),
        );
        echo form_input($dataService);
        echo br(1);
     
        
        /* Kaina nuo */
        echo form_label(lang("price_from"), 'price_from');
        $dataPriceFrom = array(
            'name' => 'price_from',
            'id' => 'price_from',
            'size' => '500',
            'value' => $row->price_from,
            'class' => 'form-control',
            'placeholder' => lang("price_from")
        );
        echo form_input($dataPriceFrom);
        echo br(1);
        
        
        /*  Kaina iki */
        echo form_label(lang("price_to"), 'price_to');
        $dataPriceTo = array(
            'name' => 'price_to',
            'id' => 'price_to',
            'rows' => '5',
            'cols' => '5',
            'value' => $row->price_to,
            'class' => 'form-control',
            'placeholder' => lang("price_to")
        );
        echo form_input($dataPriceTo);
        echo br(1);
        
         /*  Aprasymas */
        echo form_label(lang("description"), 'description');
        $dataDescription = array(
            'name' => 'description',
            'id' => 'description',
            'rows' => '5',
            'cols' => '5',
            'value' => $row->description,
            'class' => 'form-control',
            'placeholder' => lang("description")
        );
        echo form_textarea($dataDescription);
        
        echo br(3);
        
            
            echo form_fieldset_close();
            echo br(1);
            $buttonSubmit = array(
                'name' => 'mysubmit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Išsaugoti'
            );
            echo form_submit($buttonSubmit);
            echo anchor("general/services/seeServices", lang('back'),
                    array(  'name' => 'backSeeServices',
                            'id' => 'backSeeServices',
                            'class'=> 'btn btn-default'));
                
            echo form_close();
        }
        ?>
    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
