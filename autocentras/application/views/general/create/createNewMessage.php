<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('ask_q'); ?></h1>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            
    <?php
    
   
        echo validation_errors();
        
        echo form_open('/general/messages/createMessage');
        
        echo br(2);
        
        /* Vardas Pavarde */
        echo form_label(lang("fl_name"), 'fl_name');
        $dataName = array(
            'name' => 'fl_name',
            'id' => 'fl_name',
            'size' => '500',
            'value' => set_value('fl_name'),
            'class' => 'form-control',
            'placeholder' => 'Vardas Pavardė'
        );
        echo form_input($dataName);
        echo br(1);
     
        
        /* Email */
        echo form_label(lang("email"), 'email');
        $dataEmail = array(
            'name' => 'email',
            'id' => 'email',
            'size' => '500',
            'value' => set_value('email'),
            'class' => 'form-control',
            'placeholder' => 'El. Paštas'
        );
        echo form_input($dataEmail);
        echo br(1);
        
        
        /* Message */
        echo form_label(lang("message"), 'message');
        $dataMessage = array(
            'name' => 'message',
            'id' => 'message',
            'rows' => '5',
            'cols' => '5',
            'value' => set_value('message'),
            'class' => 'form-control',
            'placeholder' => 'Klausimas/pranešimas'
        );
        echo form_textarea($dataMessage);
        
        echo br(3);
        
        echo anchor("general/main/home", lang('back'),
                array(  'name' => 'backHomePage',
                        'id' => 'backHomePage',
			'class'=> 'btn btn-default'));
        echo nbs(3);
            
            $buttonSubmit = array(
            'name' => 'mysubmit',
            'id' => 'submit',
            'value' => lang('submit'),
            'class'=> 'btn btn btn-primary'
             );
            echo form_submit($buttonSubmit);
            
            
            
        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->


