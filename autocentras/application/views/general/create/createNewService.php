<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_new_service'); ?></h1>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            
    <?php
    
   
        echo validation_errors();
        
        echo form_open('/general/services/createService');
        
        echo br(2);
        
        /* Paslauga */
        echo form_label(lang("service"), 'service');
        $dataService = array(
            'name' => 'service',
            'id' => 'service',
            'size' => '500',
            'value' => set_value('service'),
            'class' => 'form-control',
            'placeholder' => lang("service_name"),
        );
        echo form_input($dataService);
        echo br(1);
     
        
        /* Kaina nuo */
        echo form_label(lang("price_from"), 'price_from');
        $dataPriceFrom = array(
            'name' => 'price_from',
            'id' => 'price_from',
            'size' => '500',
            'value' => set_value('price_from'),
            'class' => 'form-control',
            'placeholder' => lang("price_from")
        );
        echo form_input($dataPriceFrom);
        echo br(1);
        
        
        /*  Kaina iki */
        echo form_label(lang("price_to"), 'price_to');
        $dataPriceTo = array(
            'name' => 'price_to',
            'id' => 'price_to',
            'rows' => '5',
            'cols' => '5',
            'value' => set_value('price_to'),
            'class' => 'form-control',
            'placeholder' => lang("price_to")
        );
        echo form_input($dataPriceTo);
        echo br(1);
        
         /*  Aprasymas */
        echo form_label(lang("description"), 'description');
        $dataDescription = array(
            'name' => 'description',
            'id' => 'description',
            'rows' => '5',
            'cols' => '5',
            'value' => set_value('description'),
            'class' => 'form-control',
            'placeholder' => lang("description")
        );
        echo form_textarea($dataDescription);
        
        echo br(3);
        
        echo anchor("general/services/seeServices", lang('back'),
                    array(  'name' => 'backSeeServices',
                            'id' => 'backSeeServices',
                            'class'=> 'btn btn-default'));
        echo nbs(3);
            
            $buttonSubmit = array(
            'name' => 'mysubmit',
            'id' => 'submit',
            'value' => lang('submit'),
            'class'=> 'btn btn btn-primary'
             );
            echo form_submit($buttonSubmit);
            
            
            
        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->


