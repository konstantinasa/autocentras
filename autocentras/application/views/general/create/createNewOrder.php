<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_order_1'); ?>s</h1>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            
    <?php
    // --- Progres bar -------
    $done = array(
        'first' => 'progtrckr-done',
        'second' => 'progtrckr-todo',
        'third' => 'progtrckr-todo'
    );
    $this->load->view('include/progress_bar', $done);
    echo br(1);
    
        echo validation_errors();
        
        echo form_open('/general/orders/createNewOrder');
        
        echo anchor("general/main/home", "Grįžti atgal",
        array(  'name' => 'backHomePage',
                'id' => 'backHomePage',
                'class'=> 'btn btn-default'));
        echo nbs(3);

        $buttonSubmit = array(
        'name' => 'mysubmit',
        'id' => 'submit',
        'value' => 'Testi',
        'class'=> 'btn btn btn-primary'
         );
        echo form_submit($buttonSubmit);
        echo nbs(3);
        
        ?>
        <div class="pull-right">
            <?php
            echo anchor("general/orders/clear_form", lang('clear_form'),
                    array('id' => 'clear_form',
                        'name' => 'clear_form',
                        'type' => 'button',
                        'width' => '200',
                        'height' => '200',
                        'class'=> 'btn btn-danger btn-xs'));  
           ?>
        </div>
        <?php
        
        echo br(2);
        
        /* Paslauga  */
        echo form_label(lang("service") , 'paslauga');
        $dataServices = array(
            '0' => '-'
        );
        foreach ($resultServices as $row) {
            $dataServices[$row->id] = $row->service;
        }
        echo form_dropdown('paslauga', $dataServices, set_value('paslauga') === null ? set_value('paslauga') : isset($this->session->userdata['step_1']['paslauga']) ? $this->session->userdata['step_1']['paslauga'] :  set_value('paslauga'), 'id="dropDownService"', 'class="form-control"');
        echo br(2);
        
        /* Kiekis */
        echo form_label(lang("quantity"), 'kiekis');
        $dataAprasymas = array(
            'name' => 'kiekis',
            'id' => 'kiekis',
            'size' => '500',
            'value' => set_value('kiekis') === null ? set_value('kiekis') : isset($this->session->userdata['step_1']['kiekis']) ? $this->session->userdata['step_1']['kiekis'] : '1',
            'class' => 'form-control',
            'placeholder' => 'Paslaugos kiekis (pvz: 1)'
        );
        echo form_input($dataAprasymas);
        echo br(1);
        
        
        /* Aprasymas */
        echo form_label(lang("description"), 'aprasymas');
        $dataAprasymas = array(
            'name' => 'aprasymas',
            'id' => 'aprasymas',
            'size' => '500',
            'value' => set_value('aprasymas') === null ? set_value('aprasymas') : isset($this->session->userdata['step_1']['aprasymas']) ? $this->session->userdata['step_1']['aprasymas'] : set_value('aprasymas'),
            'class' => 'form-control',
            'placeholder' => 'Pastabos'
        );
        echo form_input($dataAprasymas);
        echo br(1);
        
        
        /* Atvykimo data */
        echo form_label(lang("arrival_date"), 'tdata');
        ?>
        
        <div class="input-group date form_date col-md-5" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
            <input name="tdata" id="tdata" class="form-control" size="16" type="text" value="<?php echo set_value('tdata') === null ? set_value('tdata') : isset($this->session->userdata['step_1']['tdata']) ? $this->session->userdata['step_1']['tdata'] : set_value('tdata'); ?>" readonly>
            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
       <input type="hidden" id="dtp_input2" value="" />
            
        <?php
        echo br(1);
        
        
        /* Atvykimo laikas  */
        echo form_label(lang("arrival_time") , 'atvyk_laikas');
        $dataNationality = array(
            '0' => '-'
        );
        foreach ($resultTime as $row) {
            $dataTime[$row->id] = $row->laikas;
        }
        echo form_dropdown('atvyk_laikas', $dataTime, set_value('atvyk_laikas') === null ? set_value('atvyk_laikas') : isset($this->session->userdata['step_1']['atvyk_laikas']) ? $this->session->userdata['step_1']['atvyk_laikas'] :  set_value('atvyk_laikas'), 'id="dropDownTime"', 'class="form-control"');
        echo br(2);
        
        
        echo br(3);
        
        echo anchor("general/main/home", lang('back'),
                array(  'name' => 'backHomePage',
                        'id' => 'backHomePage',
			'class'=> 'btn btn-default'));
        echo nbs(3);
            
            $buttonSubmit = array(
            'name' => 'mysubmit',
            'id' => 'submit',
            'value' => lang('proceed'),
            'class'=> 'btn btn btn-primary'
             );
            echo form_submit($buttonSubmit);
            
            
            
        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->


