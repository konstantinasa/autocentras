<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_order_2'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
    
    <?php
    // --- Progres bar -------
    $done = array(
        'first' => 'progtrckr-done',
        'second' => 'progtrckr-done',
        'third' => 'progtrckr-todo'
    );
    $this->load->view('include/progress_bar', $done);
    echo br(1);
        
        echo validation_errors();
        echo form_open('/general/orders/createNewOrder2');
        
        echo anchor("general/orders/createNewOrder", lang('back'),
        array(  'name' => 'backCreateOrder',
                'id' => 'backCreateOrder',
                'class'=> 'btn btn-default'));
        echo nbs(3);

        $buttonSubmit = array(
        'name' => 'mysubmit',
        'id' => 'submit',
        'value' => 'Testi',
        'class'=> 'btn btn btn-primary'
         );
        echo form_submit($buttonSubmit);
        echo nbs(3);

        ?>
        <div class="pull-right">
            <?php
            echo anchor("general/orders/clear_form", "Išvalyti anketą",
                    array('id' => 'clear_form',
                        'name' => 'clear_form',
                        'type' => 'button',
                        'width' => '200',
                        'height' => '200',
                        'class'=> 'btn btn-danger btn-xs'));  
           ?>
        </div>
        <?php 
       
        echo br(2);
//        echo var_dump($this->session->userdata['step_1']['kiekis']);
//        echo br(2);
//        echo var_dump($service_price);
//        echo br(2);
        
        $kiekis = $this->session->userdata['step_1']['kiekis'];
        
        foreach ($service_price as $price) {
            
            $kaina_nuo = $kiekis * $price->price_from;
            $kaina_iki = $kiekis * $price->price_to;
            
             echo "<div class='alert alert-info'>
                               Pasirinktos paslaugos " . $price->service . " preliminari kaina už vnt.: $price->price_from - $price->price_to EUR </br>
                               Jūs pasirinkote $kiekis  vnt. Jūsų preliminari kaina yra: $kaina_nuo - $kaina_iki EUR
                   </div>";
        }
        
        echo br(1);
        
         /* Automobilis */
        echo form_label(lang("car"), 'car');
        $dataCar = array(
            'name' => 'car',
            'id' => 'car',
            'size' => '100',
            'value' => set_value('car') === null ? set_value('car') : isset($this->session->userdata['step_2']['car']) ? $this->session->userdata['step_2']['car'] : set_value('car'),
            'class' => 'form-control',
            'placeholder' => 'pvz: Audi'
        );
        echo form_input($dataCar);
        echo br(1);
        
        /* Modelis */
        echo form_label(lang('model'), 'model');
        $dataModel = array(
            'name' => 'model',
            'id' => 'model',
            'size' => '100',
            'value' => set_value('model') === null ? set_value('model') : isset($this->session->userdata['step_2']['model']) ? $this->session->userdata['step_2']['model'] : set_value('model'),
            'class' => 'form-control',
            'placeholder' => 'pvz: A8'
        );
        echo form_input($dataModel);
        echo br(1);
        
         /* Valst Nr*/
        echo form_label(lang('valst_no'), 'valst_nr');
        $dataValstNr = array(
            'name' => 'valst_nr',
            'id' => 'valst_nr',
            'size' => '100',
            'value' => set_value('valst_nr') === null ? set_value('valst_nr') : isset($this->session->userdata['step_2']['valst_nr']) ? $this->session->userdata['step_2']['valst_nr'] : set_value('valst_nr'),
            'class' => 'form-control',
            'placeholder' => 'ABC 123'
        );
        echo form_input($dataValstNr);
        echo br(1);
        
        
        echo form_label(lang("production_date"), 'pdata');
        ?>
        
        <div class="input-group date form_date col-md-5" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
            <input name="production_date" id="production_date" class="form-control" size="16" type="text" value="<?php echo set_value('production_date') === null ? set_value('production_date') : isset($this->session->userdata['step_2']['production_date']) ? $this->session->userdata['step_2']['production_date'] : set_value('production_date'); ?>" readonly>
            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
       <input type="hidden" id="dtp_input2" value="" />
            
        <?php
        echo br(1);
        
        /* Draudimas*/
        echo form_label(lang('insurance'), 'insurance');
        $dataInsurance = array(
            'name' => 'insurance',
            'id' => 'insurance',
            'size' => '100',
            'value' => set_value('insurance') === null ? set_value('insurance') : isset($this->session->userdata['step_2']['insurance']) ? $this->session->userdata['step_2']['insurance'] : set_value('insurance'),
            'class' => 'form-control',
            'placeholder' => 'pvz: Kasko'
        );
        echo form_input($dataInsurance);
        echo br(3);

        
        echo anchor("general/orders/createNewOrder", lang('back'),
                array(  'name' => 'backCreateNewOrder',
                        'id' => 'backCreateNewOrder',
			'class'=> 'btn btn-default'));
        echo nbs(3);
        
        //echo var_dump($this->session->userdata['test']);
        
        
        $buttonSubmit2 = array(
            'name' => 'mysubmit2',
            'id' => 'submit2',
            'value' => lang('proceed'),
            'class'=> 'btn btn btn-primary'
        );
        echo form_submit($buttonSubmit2);
        ?>
         
           </br> 
        <?php
//        var_dump($this->session->all_userdata());
//        var_dump($this->session->userdata['step_2']); 
//        echo "</br>";
//        var_dump($this->session->userdata['step_2']);
//        echo '</br>';
//        var_dump($this->session->userdata['step_2']);
        //var_dump($dataSvorKit);
        ?>
            
           
        <?php
        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
