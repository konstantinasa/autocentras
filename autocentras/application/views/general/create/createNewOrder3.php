<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo lang('create_order_3'); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
    
    <?php
    // --- Progres bar -------
    $done = array(
        'first' => 'progtrckr-done',
        'second' => 'progtrckr-done',
        'third' => 'progtrckr-done'
    );
    $this->load->view('include/progress_bar', $done);
    echo br(1);
        
        echo validation_errors();
        echo form_open('/general/orders/createNewOrder3');
        
        echo anchor("general/orders/createNewOrder2", lang('back'),
        array(  'name' => 'backCreateOrder2',
                'id' => 'backCreateOrder2',
                'class'=> 'btn btn-default'));
        echo nbs(3);

        $buttonSubmit = array(
        'name' => 'mysubmit',
        'id' => 'submit',
        'value' => 'Testi',
        'class'=> 'btn btn btn-primary'
         );
        echo form_submit($buttonSubmit);
        echo nbs(3);

        ?>
        <div class="pull-right">
            <?php
            echo anchor("general/orders/clear_form", "Išvalyti anketą",
                    array('id' => 'clear_form',
                        'name' => 'clear_form',
                        'type' => 'button',
                        'width' => '200',
                        'height' => '200',
                        'class'=> 'btn btn-danger btn-xs'));  
           ?>
        </div>
        <?php 
       
        echo br(2);
        
        
         /* Vardas */
        echo form_label(lang("fname"), 'fname');
        $dataName = array(
            'name' => 'fname',
            'id' => 'fname',
            'size' => '100',
            'value' => set_value('fname') === null ? set_value('fname') : isset($this->session->userdata['step_3']['fname']) ? $this->session->userdata['step_3']['fname'] : set_value('fname'),
            'class' => 'form-control',
            'placeholder' => ''
        );
        echo form_input($dataName);
        echo br(1);
        
        /* Pavarde */
        echo form_label(lang('lname'), 'lname');
        $dataLName = array(
            'name' => 'lname',
            'id' => 'lname',
            'size' => '100',
            'value' => set_value('lname') === null ? set_value('lname') : isset($this->session->userdata['step_3']['lname']) ? $this->session->userdata['step_3']['lname'] : set_value('lname'),
            'class' => 'form-control',
            'placeholder' => ''
        );
        echo form_input($dataLName);
        echo br(1);
        
         /* Tel nr*/
        echo form_label(lang('tel_no'), 'tel_no');
        $dataValstNr = array(
            'name' => 'tel_no',
            'id' => 'tel_no',
            'size' => '100',
            'value' => set_value('tel_no') === null ? set_value('tel_no') : isset($this->session->userdata['step_3']['tel_no']) ? $this->session->userdata['step_3']['tel_no'] : set_value('tel_no'),
            'class' => 'form-control',
            'placeholder' => ''
        );
        echo form_input($dataValstNr);
        echo br(1);
        
        /* El Pastas*/
        echo form_label(lang('email'), 'email');
        $dataEmail = array(
            'name' => 'email',
            'id' => 'email',
            'size' => '100',
            'value' => set_value('email') === null ? set_value('email') : isset($this->session->userdata['step_3']['email']) ? $this->session->userdata['step_3']['email'] : set_value('email'),
            'class' => 'form-control',
            'placeholder' => ''
        );
        echo form_input($dataEmail);
        echo br(3);

        
        echo anchor("general/orders/createNewOrder2", lang('back'),
                array(  'name' => 'backCreateNewOrder2',
                        'id' => 'backCreateNewOrder2',
			'class'=> 'btn btn-default'));
        echo nbs(3);
        
        $buttonSubmit2 = array(
            'name' => 'mysubmit2',
            'id' => 'submit2',
            'value' => lang('proceed'),
            'class'=> 'btn btn btn-primary'
        );
        echo form_submit($buttonSubmit2);
        ?>
         
           </br> 
        <?php
//        var_dump($this->session->all_userdata());
//        var_dump($this->session->userdata['step_3']); 
//        echo "</br>";
//        var_dump($this->session->userdata['step_3']);
//        echo '</br>';
//        var_dump($this->session->userdata['step_1']);
        //var_dump($dataSvorKit);
        ?>
            
           
        <?php
        echo form_close();
        ?>

    </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
