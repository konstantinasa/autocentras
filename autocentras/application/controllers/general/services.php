<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller
{
    
     function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/services_mod');
        $this->load->model('general/otherInformation');
        $this->load->library("pagination");
        
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language', 'form'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        
    }
    
     public function seeServices($page = 0){
        
        if (!$this->ion_auth->logged_in()) {             
                redirect('auth', 'refresh');
                }
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');
            $config = array();
            $config["base_url"] = base_url() . "general/services/seeServices";
            $config["total_rows"] = $this->services_mod->record_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            $data = array(
                'title' => lang("all_services"),
                'firstFolder' => "general/view",
                'main_content' => "seeServices",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            
            $data["resultAllServices"] = $this->services_mod->fetch_services($config["per_page"], $page);

            
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		}  else {
                     redirect('auth', 'refresh');
                } 
    }
    
    
    public function createService() {
        
        $session_data = $this->session->userdata('logged_in');
            
        $data['username'] = $this->session->userdata('username');

        $data = array(
            'title' => "Pridėti paslaugą",
            'firstFolder' => "general/create",
            'main_content' => "createNewService",
            'username' => $this->session->userdata('username')
        );
        
        $this->form_validation->set_rules('service', lang("service"), 'required|max_length[499]');
        $this->form_validation->set_rules('price_from', lang("price_from"), 'required|greater_than[0]');
        $this->form_validation->set_rules('price_to', lang("price_to"), 'greater_than[0]');
        $this->form_validation->set_rules('description', lang("description"), 'max_length[999]');
        
        
        if ($this->form_validation->run() == FALSE) {
                
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		}  else {
                     redirect('auth', 'refresh');
                } 
            } else {
                
                //echo 'done';
                
                $services_data = array(
                    'service' => $this->clean($this->input->post('service')),
                    'price_from' => $this->clean($this->input->post('price_from')),
                    'price_to' => $this->input->post('price_to') == '' ? 'null' : $this->clean($this->input->post('price_to')),
                    'description' => $this->input->post('description') == '' ? ' ' : $this->clean($this->input->post('description')),
                );
                
                $this->services_mod->insertService($services_data);
                
                $urll = base_url("/general/services/seeServices"); 
                header("Refresh: 1, url=$urll");
                echo '<center><p style="font-size:33px"> Atlikta! </p></center>';

            }
        
    }
    
    public function delete_service($id) {
        
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {           
            redirect('auth', 'refresh');
        }
            $this->services_mod->deleteService($id);
            
            $urll = base_url("/general/services/seeServices"); 
            header("Refresh: 1, url=$urll");
            echo '<center><p style="font-size:33px"> Atlikta! </p></center>';
        
    }
    
    public function edit_service($id){
        
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            $data = array(
                'title' => lang('edit_service'),
                'firstFolder' => "general/edit",
                'main_content' => "editService",
                'username' => $this->session->userdata('username')
            );
            
            $data['resultServices'] = $this->services_mod->editService($id);
            
            $this->form_validation->set_rules('service', lang("service"), 'required');
            $this->form_validation->set_rules('price_from', lang("price_from"), 'required|greater_than[0]');
            $this->form_validation->set_rules('price_to', lang("price_to"), 'greater_than[0]');
            $this->form_validation->set_rules('description', lang("description"), 'max_length[999]');

            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		}
            } else {
                $services_data = array(
                    'service' => $this->clean($this->input->post('service')),
                    'price_from' => $this->clean($this->input->post('price_from')),
                    'price_to' => $this->input->post('price_to') == '' ? null : $this->clean($this->input->post('price_to')),
                    'description' => $this->input->post('description') == '' ? ' ' : $this->clean($this->input->post('description')),
                );
                
                $this->services_mod->updateService($id, $services_data);
                
                $urll = base_url("/general/services/seeServices"); 
                header("Refresh: 1, url=$urll");
                echo '<center><p style="font-size:33px"> Atlikta! </p></center>';
            }
    }    
    
    
    
    
    
    
    
    
    
    
   public function clean($string){
        $string = str_replace('"', "", $string);
        $string = str_replace("'", "", $string);
        return $string;
    } 
    
    
}