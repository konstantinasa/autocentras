<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/orders_mod');
        $this->load->model('general/otherInformation');
        $this->load->library("pagination");
        
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language', 'form'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        
    }

    
    public function seeOrders($page = 0){
        
        if (!$this->ion_auth->logged_in()) {             
                redirect('auth', 'refresh');
                }
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');
            $config = array();
            $config["base_url"] = base_url() . "general/orders/seeOrders";
            $config["total_rows"] = $this->orders_mod->record_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            $data = array(
                'title' => lang("all_orders"),
                'firstFolder' => "general/view",
                'main_content' => "seeOrders",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            
            $data["resultAllOrders"] = $this->orders_mod->fetch_orders($config["per_page"], $page);

            
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} else {
                     redirect('auth', 'refresh');
                } 
    }
    
    
    
    
    
    public function createNewOrder(){
                
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');

            $data = array(
                'title' => "Užsisakyti paslaugą",
                'firstFolder' => "general/create",
                'main_content' => "createNewOrder",
                'username' => $this->session->userdata('username')
            );
            
            
            $this->form_validation->set_rules('paslauga', lang("service"), '');
            $this->form_validation->set_rules('kiekis', lang("quantity"), 'required|integer|greater_than[0]');
            $this->form_validation->set_rules('aprasymas', lang("description"), 'callback_clean');
            $this->form_validation->set_rules('tdata', lang("arrival_date"), 'required');
            $this->form_validation->set_rules('atvyk_laikas', lang("arrival_time"), 'required|callback_check_time');

            $data["resultServices"] = $this->otherInformation->allServices();
            $data["resultTime"] = $this->otherInformation->allTime();
            

            

            if ($this->form_validation->run() == FALSE) {
                
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('user')) 
		{
		     $this->load->view('template/template_user', $data);
		}
		else
		{
		     $this->load->view('template/template_guest', $data);
		}
            } else {
                
                $step_1 = array(
                    'paslauga' => ($this->input->post('paslauga')),
                    'kiekis' => ($this->input->post('kiekis')),
                    'aprasymas' => $this->clean($this->input->post('aprasymas')),
                    'tdata' => ($this->input->post('tdata')),
                    'atvyk_laikas' => ($this->input->post('atvyk_laikas')),
                    
                );
                
                $test = $this->check_time();
                $this->session->set_userdata('test', $test);
                
                $this->session->set_userdata('step_1', $step_1);
                
                redirect('/general/orders/createNewOrder2', 'refresh');
            }
        
    }

    
    public function createNewOrder2(){
                
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');

            $data = array(
                'title' => "Užsisakyti paslaugą",
                'firstFolder' => "general/create",
                'main_content' => "createNewOrder2",
                'username' => $this->session->userdata('username')
            );
            
            
            $this->form_validation->set_rules('car', lang("car"), 'callback_clean');
            $this->form_validation->set_rules('model', lang("model"), 'callback_clean');
            $this->form_validation->set_rules('valst_nr', lang("valst_no"), 'callback_clean');
            $this->form_validation->set_rules('production_date', lang("production_date"), 'callback_clean');
            $this->form_validation->set_rules('insurance', lang("insurance"), 'trim|prep_for_form|xss_clean|callback_clean');
            
            $data['service_price'] = $this->otherInformation->service_price($this->session->userdata['step_1']['paslauga']);
            

            if ($this->form_validation->run() == FALSE) {
                
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('user')) 
		{
		     $this->load->view('template/template_user', $data);
		}
		else
		{
		     $this->load->view('template/template_guest', $data);
		}
            } else {
                
                $step_2 = array(
                    'car' => $this->clean($this->input->post('car')),
                    'model' => $this->clean($this->input->post('model')),
                    'valst_nr'=> $this->clean($this->input->post('valst_nr')),
                    'production_date'=> $this->clean($this->input->post('production_date')),
                    'insurance'=> $this->clean($this->input->post('insurance')),
                    
                );
             
                $this->session->set_userdata('step_2', $step_2);
                
                redirect('/general/orders/createNewOrder3', 'refresh');
            }
        
    }
    
    
    
    public function createNewOrder3(){
                
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');

            $data = array(
                'title' => "Užsisakyti paslaugą",
                'firstFolder' => "general/create",
                'main_content' => "createNewOrder3",
                'username' => $this->session->userdata('username')
            );
            
            
            $this->form_validation->set_rules('fname', lang("fname"),'trim|prep_for_form|xss_clean|callback_clean');
            $this->form_validation->set_rules('lname', lang("lname"), 'trim|prep_for_form|xss_clean|callback_clean');
            $this->form_validation->set_rules('tel_no', lang("tel_no"), 'trim|prep_for_form|xss_clean|callback_clean');
            $this->form_validation->set_rules('email', lang("email"), 'valid_email|trim|prep_for_form|xss_clean|callback_clean');
            

            if ($this->form_validation->run() == FALSE) {
                
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('user')) 
		{
		     $this->load->view('template/template_user', $data);
		}
		else
		{
		     $this->load->view('template/template_guest', $data);
		}
            } else {
                
                $step_3 = array(
                    'fname' => $this->clean($this->input->post('fname')),
                    'lname' => $this->clean($this->input->post('lname')),
                    'tel_no'=> $this->clean($this->input->post('tel_no')),
                    'email'=> $this->clean($this->input->post('email')), 
                );
             
                $this->session->set_userdata('step_3', $step_3);
                
               $step_1 = $this->session->userdata['step_1'];
               $step_2 = $this->session->userdata['step_2'];
               $step_3 = $this->session->userdata['step_3'];
               
               $this->orders_mod->insertOrder($step_1, $step_2, $step_3);
               
               $this->session->unset_userdata('step_1');
               $this->session->unset_userdata('step_2');
               $this->session->unset_userdata('step_3');
  
               redirect('/general/orders/order_succeed', 'refresh');
               
            }
        
    }
    

    public function check_time() {
        $date = $this->input->post('tdata');
        $time = $this->input->post('atvyk_laikas');
        $max_services = 1;
        if($date !== ''){
            $times_count = $this->orders_mod->time_check($date, $time);
            $count = array();
            foreach ($times_count as $times){
                $count = $times->count;
            }
            if($count[0] < $max_services) {
                return true;
            } else {
                $this->form_validation->set_message('check_time', '<div class="alert alert-danger"> Šis laikas jau yra užimtas, pasirinkite kitą laiką arba datą. </div>');
                return false;
            }
        }
    }   
    
    
    public function edit_order($id){
        
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {            
            redirect('auth', 'refresh');
        }
            $session_data = $this->session->userdata('logged_in');
            
            $data = array(
                'title' => lang('edit_order'),
                'firstFolder' => "general/edit",
                'main_content' => "editOrder",
                'username' => $this->session->userdata('username')
            );
            
            $data['resultsOrders'] = $this->orders_mod->editOrder($id);
            $data["resultServices"] = $this->otherInformation->allServices();
            $data["resultTime"] = $this->otherInformation->allTime();
            
            $this->form_validation->set_rules('paslauga', lang("service"), '');
            $this->form_validation->set_rules('aprasymas', lang("description"), 'callback_clean');
            $this->form_validation->set_rules('tdata', lang("arrival_date"), 'required');
            $this->form_validation->set_rules('atvyk_laikas', lang("arrival_time"), 'required');
            $this->form_validation->set_rules('car', lang("car"), 'callback_clean');
            $this->form_validation->set_rules('model', lang("model"), 'callback_clean');
            $this->form_validation->set_rules('valst_nr', lang("valst_no"), 'callback_clean');
            $this->form_validation->set_rules('production_date', lang("production_date"), 'trim|prep_for_form|xss_clean');
            $this->form_validation->set_rules('insurance', lang("insurance"), 'callback_clean');
            $this->form_validation->set_rules('fname', lang("fname"),'trim|prep_for_form|xss_clean|callback_clean');
            $this->form_validation->set_rules('lname', lang("lname"), 'trim|prep_for_form|xss_clean|callback_clean');
            $this->form_validation->set_rules('tel_no', lang("tel_no"), 'trim|prep_for_form|xss_clean|callback_clean');
            $this->form_validation->set_rules('email', lang("email"), 'valid_email|trim|prep_for_form|xss_clean|callback_clean');
            
            if ($this->form_validation->run() == FALSE) {
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		}
            } else {
                $dataOrder = array(
                    'service' => ($this->input->post('paslauga')),
                    'service_descr_cust' => $this->clean($this->input->post('aprasymas')),
                    'atvykimo_data' => ($this->input->post('tdata')),
                    'atvykimo_laikas' => ($this->input->post('atvyk_laikas')),
                    'automobilis' => $this->clean($this->input->post('car')),
                    'marke' => $this->clean($this->input->post('model')),
                    'valst_nr'=> $this->clean($this->input->post('valst_nr')),
//                    'production_date'=> $this->clean($this->input->post('production_date')),
                    'draudimas'=> $this->clean($this->input->post('insurance')),
                    'vardas' => $this->clean($this->input->post('fname')),
                    'pavarde' => $this->clean($this->input->post('lname')),
                    'tel_nr'=> $this->clean($this->input->post('tel_no')),
                    'el_pastas'=> $this->clean($this->input->post('email')),
                );
                
                $this->orders_mod->updateOrder($id, $dataOrder);
                redirect('/general/orders/seeOrders', 'refresh');
            }
    }
    
    function delete_order($id)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {           
            redirect('auth', 'refresh');
        }
            $this->orders_mod->deleteOrder($id);
            redirect('/general/orders/seeOrders', 'refresh');
    }

    

    public function order_succeed(){
        
            $data = array(
                'title' => "Paslauga sekmingai užsakyta",
                'firstFolder' => "general/view",
                'main_content' => "orderSucceed",
                'username' => $this->session->userdata('username')
            );
            

            if ($this->form_validation->run() == FALSE) {
                
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('user')) 
		{
		     $this->load->view('template/template_user', $data);
		}
		else
		{
		     $this->load->view('template/template_guest', $data);
		}
            
            }
        
        
    }
     
    
    public function clean($string){
        $string = str_replace('"', "", $string);
        $string = str_replace("'", "", $string);
        return $string;
    }
    
    public function clear_form(){
        
        $this->session->unset_userdata('step_1');
        $this->session->unset_userdata('step_2');
        $this->session->unset_userdata('step_3');

         redirect('general/orders/createNewOrder', 'refresh');
    }
    
    
}


