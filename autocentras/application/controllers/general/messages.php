<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->model('general/messages_mod');
        $this->load->model('general/otherInformation');
        $this->load->library("pagination");
        
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language', 'form'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        
    }
    
    
    
    public function createMessage() {
        
        $session_data = $this->session->userdata('logged_in');
            
        $data['username'] = $this->session->userdata('username');

        $data = array(
            'title' => "Užduoti klausimą",
            'firstFolder' => "general/create",
            'main_content' => "createNewMessage",
            'username' => $this->session->userdata('username')
        );
        
        $this->form_validation->set_rules('fl_name', lang("fl_name"), 'required');
        $this->form_validation->set_rules('email', lang("email"), 'required|valid_email');
        $this->form_validation->set_rules('message', lang("message"), 'required');

        
        
        if ($this->form_validation->run() == FALSE) {
                
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('user')) 
		{
		     $this->load->view('template/template_user', $data);
		}
		else
		{
		     $this->load->view('template/template_guest', $data);
		}
            } else {
                
                //echo 'done';
                
                $message_data = array(
                    'fl_name' => $this->clean($this->input->post('fl_name')),
                    'email' => $this->clean($this->input->post('email')),
                    'message' => $this->clean($this->input->post('message'))
                );
                
                $this->messages_mod->insertMessage($message_data);
                
                $urll = base_url("/general/main/home"); 
                header("Refresh: 3, url=$urll");
                echo '<center><p style="font-size:33px"> Pranešimas sekmingai išsiūstas! </p></center>';

            }
        
    }
    
    
    public function displayMessages() {
        
        if (!$this->ion_auth->logged_in()) {             
                redirect('auth', 'refresh');
                }
            $session_data = $this->session->userdata('logged_in');
            
            $data['username'] = $this->session->userdata('username');
            $config = array();
            $config["base_url"] = base_url() . "general/messages/displayMessages";
            $config["total_rows"] = $this->messages_mod->record_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            $data = array(
                'title' => lang("all_messages"),
                'firstFolder' => "general/view",
                'main_content' => "seeMessages",
                'username' => $this->session->userdata('username')
            );
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            
            $data["resultAllMessages"] = $this->messages_mod->fetch_messages($config["per_page"], $page);

            
            $data["links"] = $this->pagination->create_links();
            
                if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		}  else {
                     redirect('auth', 'refresh');
                } 
        
    }
    
    public function delete_message($id) {
        
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {           
            redirect('auth', 'refresh');
        }
            $this->messages_mod->deleteMessage($id);
            redirect('/general/messages/displayMessages', 'refresh');
        
    }
    
    
    
    public function clean($string){
        $string = str_replace('"', "", $string);
        $string = str_replace("'", "", $string);
        return $string;
    }
    
    

    
    
    
    
}


