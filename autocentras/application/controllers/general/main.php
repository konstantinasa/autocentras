<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper("url");
        $this->load->library("pagination");
        
         $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));
        $this->lang->load('include');

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
    }


    function index()
    {
        if ($this->session->userdata('logged_in') and $this->session->userdata('logged_in')['roles'] === 'user' OR $this->session->userdata('logged_in')['roles'] === 'admin' OR $this->session->userdata('logged_in')['roles'] === 'superAdmin') {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $this->session->userdata('username');
            $data = array(
                'title' => "Autocentras",
                'firstFolder' => "general/view",
                'main_content' => "seeMain",
                'username' => $this->session->userdata('username')
            );
            if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('user')) 
		{
		     $this->load->view('template/template_user', $data);
		}
                else
		{
		     $this->load->view('template/template_guest', $data);
		}
        } else {
            redirect('login', 'refresh');
        }
    }

    
    public function home()
    {
       
            
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $this->session->userdata('username');
            
//            if (!$this->ion_auth->logged_in()) {
//                 redirect('auth', 'refresh');
//            }
            
            
            $data = array(
                'title' => "Autocentras",
                'firstFolder' => "general/view",
                'main_content' => "seeMain",
                'username' => $this->session->userdata('username')
            );
            
            if ($this->ion_auth->is_admin())
		{
		     $this->load->view('template/template_admin', $data);
		} 
		elseif ($this->ion_auth->in_group('user')) 
		{
		     $this->load->view('template/template_user', $data);
		}
		else
		{
		     $this->load->view('template/template_guest', $data);
		}
                
       
    }

}


