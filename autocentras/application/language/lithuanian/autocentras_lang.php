<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['main_page']='Pagrindinis puslapis';
$lang['service_order']='Užsisakyti paslaugą';
$lang['about_us']='Apie mus';
$lang['contacts']='Kontaktai';

$lang['service']='Paslauga';
$lang['description']='Aprašymas';
$lang['arrival_date']='Atvykimo data';
$lang['arrival_time']='Atvykimo laikas';
$lang['car']='Automobilis';
$lang['model']='Marke';
$lang['valst_no']='Valstybinis numeris';
$lang['insurance']='Draudimas';
$lang['production_date']='Pagaminimo data';
$lang['service']='Paslauga';
$lang['back']='Grįžti atgal';
$lang['proceed']='Testi';
$lang['clear_form']='Išvalyti anketą';
$lang['create_order_1']='Užsisakyti paslaugą 1 žingsnis';
$lang['create_order_2']='Užsisakyti paslaugą 2 žingsnis';
$lang['create_order_3']='Užsisakyti paslaugą 3 žingsnis';
$lang['orderer']='Užsakovas';
$lang['fname']='Vardas';
$lang['lname']='Pavardė';
$lang['tel_no']='Telefono numeris';
$lang['email']='El. paštas';
$lang['order_suceed']='Užsakymas sekmingas';
$lang['main_page']='Pagrindinis puslapis';
$lang['header']='Autocentras';
$lang['see_orders']='Užsakymai';
$lang['all_orders']='Visi užsakymai';
$lang['add_info']='Pildyti informaciją';
$lang['services_list']='Paslaugų sąrašas';
$lang['times_list']='Laikų sąrašas';
$lang['all_orders']='Visi užsakymai';
$lang['create_order']='Sukurti užsakymą';
$lang['order_info_1']='Užsakymo info 1';
$lang['order_info_2']='Užsakymo info 2';
$lang['order_info_3']='Užsakymo info 3';
$lang['order_id']='Užsakymo ID';
$lang['service_name']='Paslaugos pavadinimas';
$lang['notes']='Pastabos';
$lang['arrival_time']='Atvykimo laikas';
$lang['arrival_date']='Atvykimo data';
$lang['car_name']='Automobilis';
$lang['car_model']='Modelis';
$lang['car_no']='Valst. Nr.';
$lang['car_insurance']='Draudimas';
$lang['no_information']='Informacijos nėra';
$lang['action_edit']='Redaguoti';
$lang['action_delete']='Ištrinti';
$lang['edit_order']='Redaguoti užsakymą';
$lang['order']='Užsakymas';
$lang['users_management']='Vartotojų valdymas';
$lang['quantity']='Paslaugos kiekis(vnt)';
$lang['leave_message']='Palikti pranešimą';
$lang['fl_name']='Vardas Pavardė';
$lang['message']='Pranešimas/klausimas';
$lang['ask_q']='Užduoti klausimą';
$lang['submit']='Pateikti';
$lang['all_messages']='Vartotojų pranešimai';
$lang['message_id']='ID';
$lang['user_name']='Vardas Pavardė';
$lang['price_from']='Kaina nuo';
$lang['price_to']='Kaina iki';
$lang['edit_service']='Redaguoti paslaugą';
$lang['create_new_service']='Sukurti naują paslaugą';
$lang['service_id']='Paslaugos ID';
$lang['all_services']='Visos paslaugos';
$lang['create_service']='Sukurti naują paslaugą';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';
$lang['template']='template';










//$lang['file_search']='Ieškoti';


