<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['include_create_user_group'] = 'Vartotojo ir grupės nustatymai';
$lang['include_user_list'] = 'Vartotojų sarašas';
$lang['include_group_list'] = 'Grupių sarašas';
$lang['include_user_information'] = 'Vartotojo informacija';
$lang['include_change_password'] = 'Pasikeisti slaptažodį';
$lang['include_welcome']='Sveiki prisijungę, %s';


$lang['register'] = 'Registruotis';
$lang['sign_in'] = 'Prisijungti';



