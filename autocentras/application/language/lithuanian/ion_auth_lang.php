<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - Lithuanian (UTF-8)
*
* Author: Ben Edmunds
* 		  ben.edmunds@gmail.com
*         @benedmunds
* Translation:  Radas7
*             radas7@gmail.com
*
*
* Created:  2012-03-04
*
* Description:  Lithuanian language file for Ion Auth messages and errors
*
*/

// Account Creation
$lang['account_creation_successful'] 	  	 = 'Vartotojas sėkmingai sukurtas';
$lang['account_creation_unsuccessful'] 	 	 = 'Neįmanoma sukurti vartotojo';
$lang['account_creation_duplicate_email'] 	 = 'El. pašto adresas jau yra arba neteisingas';
$lang['account_creation_duplicate_username'] 	 = 'Prisijungimo vardas jau yra arba nekorektiškas';

// TODO Please Translate
$lang['account_creation_missing_default_group'] = 'Įprasta grupė nenustatyta.';
$lang['account_creation_invalid_default_group'] = 'Neteisingai nustatyta grupė rinkinys.';

// Password
$lang['password_change_successful'] 	 	 = 'Slaptažodis sukurtas';
$lang['password_change_unsuccessful'] 	  	 = 'Negalima pakeisti slaptažodžio';
$lang['forgot_password_successful'] 	 	 = 'Slaptažodis keičiamas. Instrukcijos išsiųstos paštu.';
$lang['forgot_password_unsuccessful'] 	 	 = 'Neįmanoma pakeisti slaptažodžio';

// Activation
$lang['activate_successful'] 		  	 = 'Vartotojas aktyvuotas';
$lang['activate_unsuccessful'] 		 	 = 'Nepavyko aktyvuoti';
$lang['deactivate_successful'] 		  	 = 'Deaktyvuotas';
$lang['deactivate_unsuccessful'] 	  	 = 'Neįmanoma deaktyvuoti';
$lang['activation_email_successful'] 	  	 = 'Išsiųstas pranešimas į el. paštą';
$lang['activation_email_unsuccessful']   	 = 'Neįmanoma išsiųsti';

// Login / Logout
$lang['login_successful'] 		  	 = 'Sėkminga autorizacija';
$lang['login_unsuccessful'] 		  	 = 'Klaidingas prisijungimas';
$lang['login_unsuccessful_not_active'] 		 = 'Vartotojas neaktyvus';
$lang['login_timeout']                       = 'Laikinai užrakintas. Bandykite vėliau.';
$lang['logout_successful'] 		 	 = 'Atsijungta sėkmingai';

// Account Changes
$lang['update_successful'] 		 	 = 'Vartotojo duomenys sėkmingai pakeisti';
$lang['update_unsuccessful'] 		 	 = 'Neįmanoma pakeisti vartotojo duomenų';
$lang['delete_successful'] 		 	 = 'Vartotojas pašalintas';
$lang['delete_unsuccessful'] 		 	 = 'Neįmanoma pašalinti vartotojo';

// Groups
$lang['group_creation_successful']  = 'Grupė duomenys sėkmingai sukurti';
$lang['group_already_exists']       = 'Toks grupės pavadinimas jau yra';
$lang['group_update_successful']    = 'Grupės duomenys atnaujinti';
$lang['group_delete_successful']    = 'Grupė duomenys ištrinti';
$lang['group_delete_unsuccessful'] 	= 'Nepavyko ištrinti grupės';
$lang['group_delete_notallowed']    = 'Negalima ištrinti administratoriaus grupės';
$lang['group_name_required'] 		= 'Grupės pavadinimos laukas privalomas';
$lang['group_name_admin_not_alter'] = 'Administatoriaus grupės pavadinimo keisti negalima';

// Activation Email
$lang['email_activation_subject']  = 'Paskyros aktyvavimas';
$lang['email_activate_heading']    = 'Aktyvuoti vartotoją %s';
$lang['email_activate_subheading'] = 'Prašome paspausti šią nuorodą %s';
$lang['email_activate_link']       = 'Aktyvuoti Jūsų vartotoją';
// Forgot Password Email
$lang['email_forgotten_password_subject']    = 'Pamiršto slaptažodžio patvirtinimas';
$lang['email_forgot_password_heading']    = 'Atstatyti slaptažodį vartotojui %s';
$lang['email_forgot_password_subheading'] = 'Prašome paspausti šią nuorodą %s.';

// New Password Email
$lang['email_new_password_subject']    = 'Naujas slaptažodis';
$lang['email_new_password_heading']    = 'Naujas slaptažodis vartotojui %s';
$lang['email_new_password_subheading'] = 'Jūsų slaptažodis buvo atstatytas: %s';
