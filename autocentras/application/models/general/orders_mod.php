<?php

Class Orders_mod extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_services = 'services';
    protected $table_orders = 'orders';

//    protected $ss = 'ss';

    public function record_count()
    {
        return $this->db->count_all($this->table_orders);
    }

        
    function insertOrder($step_1, $step_2, $step_3){
        
        $this->db->trans_start();
        
        $this->db->query("INSERT INTO $this->table_orders (service_descr_cust, atvykimo_data, atvykimo_laikas, automobilis, marke, valst_nr, draudimas, vardas, pavarde, tel_nr, el_pastas, service, kiekis) VALUES
                                              ('" . $step_1['aprasymas'] . "', '" . $step_1['tdata'] . "', " . $step_1['atvyk_laikas'] . ", '" . $step_2['car'] . "', '" . $step_2['model'] . "', '" . $step_2['valst_nr'] . "', '" . $step_2['insurance'] . "', '" . $step_3['fname'] . "', '" . $step_3['lname'] . "', '" . $step_3['tel_no'] . "', '" . $step_3['email'] . "', " . $step_1['paslauga'] . ", " . $step_1['kiekis'] . ");");
        $this->db->trans_complete();
        
    }
    
    
    function fetch_orders($limit, $offset){
        
        $query = $this->db->query("
                                   SELECT 

                                    o.id,
                                    ser.service,
                                    o.service_descr_cust,
                                    tim.laikas,
                                    o.atvykimo_data,
                                    o.automobilis,
                                    o.marke,
                                    o.valst_nr,
                                    o.draudimas,
                                    o.vardas,
                                    o.pavarde,
                                    o.tel_nr,
                                    o.el_pastas


                                    FROM

                                            orders AS o
                                            LEFT JOIN services AS ser ON ser.id = o.service
                                            LEFT JOIN times AS tim ON tim.id = o.atvykimo_laikas

                                    WHERE

                                            o.service = ser.id AND
                                            o.atvykimo_laikas = tim.id

                                    GROUP BY
                                    o.id,
                                    ser.service,
                                    tim.laikas

                                    ORDER BY

                                            o.id

                                    LIMIT $limit
                                    OFFSET $offset
            ");
        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
        
    }
    
    public function editOrder($id) {
        
        $query = $this->db->query("
                                   SELECT 

                                            o.id,
                                            o.service,
                                            o.service_descr_cust,
                                            o.atvykimo_laikas,
                                            o.atvykimo_data,
                                            o.automobilis,
                                            o.marke,
                                            o.valst_nr,
                                            o.draudimas,
                                            o.vardas,
                                            o.pavarde,
                                            o.tel_nr,
                                            o.el_pastas

                                    FROM
                                            orders AS o
                                            LEFT JOIN services AS ser ON ser.id = o.service
                                            LEFT JOIN times AS tim ON tim.id = o.atvykimo_laikas

                                    WHERE
                                            o.service = ser.id AND
                                            o.atvykimo_laikas = tim.id AND
                                            o.id = $id

                                    GROUP BY
                                            o.id



            ");
        
        return $query->result();
                
    }
    
    
    function updateOrder($id, $dataOrder)
    {
        $this->db->update($this->table_orders, $dataOrder, array('id' => $id));
    }
    
    function deleteOrder($id)
    {
        $this->db->delete($this->table_orders, array('id' => $id));
    }        
    
    
    function time_check($date, $time){
        
            $query = $this->db->query("
                    SELECT count(o.atvykimo_laikas)
                    FROM orders AS o
                    WHERE o.atvykimo_data = '$date'
                       AND o.atvykimo_laikas = $time
                ");
        return $query->result();
                    
    }
    
    
    
    
    }

    
  

?>
