<?php

Class Services_mod extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_services = 'services';
    
     public function record_count()
    {
        return $this->db->count_all($this->table_services);
    }
        
    function insertService($service_data){
        
        $this->db->trans_start();
        
        $this->db->query("INSERT INTO $this->table_services (service, description, price_from, price_to) VALUES
                         ('" . $service_data['service'] . "', '" . $service_data['description'] . "', '" . $service_data['price_from'] . "', " . $service_data['price_to'] . ");");
        
        $this->db->trans_complete();
        
    }

    function fetch_services($limit, $offset) {
        
        $query = $this->db->query("
                                    SELECT
                                            ID,
                                            service,
                                            description,
                                            price_from,
                                            price_to
                                    FROM
                                            services
                                    ORDER BY
                                            ID
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");
        
        return $query->result();
        
    }
    
    function deleteService($id)
    {
        $this->db->delete($this->table_services, array('id' => $id));
    } 
    
    public function editService($id) {
        
        $query = $this->db->query("
                                   SELECT 
                                            id,
                                            service,
                                            description,
                                            price_from,
                                            price_to
                                    FROM
                                            services

                                    WHERE
                                           id = $id

                                    ORDER BY
                                            id
                                    ");
        
        return $query->result();
                
    }
    
    
    function updateService($id, $dataService)
    {

        $this->db->update($this->table_services, $dataService, array('id' => $id));
    }
    
    
    
    
    
    
    }

    
  

?>
