<?php

Class Messages_mod extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    protected $table_messages = 'messages';
    
     public function record_count()
    {
        return $this->db->count_all($this->table_messages);
    }
        
    function insertMessage($message_data){
        
        $this->db->trans_start();
        
        $this->db->query("INSERT INTO $this->table_messages (message, fl_name, email, date_time) VALUES
                         ('" . $message_data['message'] . "', '" . $message_data['fl_name'] . "', '" . $message_data['email'] . "', now());");
        
        $this->db->trans_complete();
        
    }

    function fetch_messages($limit, $offset) {
        
        $query = $this->db->query("
                                    SELECT
                                            ID,
                                            message,
                                            fl_name,
                                            email,
                                            date_time
                                    FROM
                                            messages
                                    ORDER BY
                                            ID
                                    LIMIT $limit 
                                    OFFSET $offset
                                    ");
        
        return $query->result();
        
    }
    
    function deleteMessage($id)
    {
        $this->db->delete($this->table_messages, array('id' => $id));
    }        
    
    
    
    
    
    
    }

    
  

?>
