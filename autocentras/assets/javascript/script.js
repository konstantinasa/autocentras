function startTime() {
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth() + 1;
    var days = d.getDate();
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var seconds = d.getSeconds();
    hours = checkTimeAndDate(hours);
    minutes = checkTimeAndDate(minutes);
    seconds = checkTimeAndDate(seconds);
    month = checkTimeAndDate(month);
    days = checkTimeAndDate(days);
    document.getElementById('clock').innerHTML = year
    + "-" + month + "-" + days + " " + hours + ":"
    + minutes + ":" + seconds;
    t = setTimeout(function () {
        startTime();
    }, 500);
}

function checkTimeAndDate(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

$(document).ready(function () {
    $(".links").click(function () {
        $(this).find("ul").toggle();
    });
});

$(document).ready(function () {
    $("#dropDownService").select2(
								{
									width: '100%',
									minimumResultsForSearch: Infinity
								});
});

$(document).ready(function () {
    $("#dropDownTime").select2(
								{
									width: '100%',
									minimumResultsForSearch: Infinity
								});
});

$(document).ready(function () {
    $("#dropDownSearchGender").select2(
								{
									width: '100%',
									minimumResultsForSearch: Infinity
								});
});

$(document).ready(function () {
    $("#dropDownSearchMunicipalityOfResidence").select2();
});

$(document).ready(function () {
    $("#dropDownSearchCity").select2({
										width: '100%',
										minimumInputLength: 4
									});
});

$(document).ready(function () {
    $("#dropDownSearchEmployment").select2(
								{
									width: '100%',
									minimumResultsForSearch: Infinity
								});
});

$(document).ready(function () {
    $("#dropDownSearchResidence").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchSerumoTag").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchSerumoDtl").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchCholKorekcija").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchFizinisAktyvum").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchAksSistNeNorm").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchAksDiastNeNm").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchKvepTkLigDaz").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchSampleId").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchAboutPatientId").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchSampleId").select2({width: '100%'});
});

$(document).ready(function () {
    $("#dropDownSearchPatient").select2({width: '100%'}
    );
});

$(document).ready(function () {
    $("#dropDownSearchSampleId").select2({width: '100%'}
    );
});

$(document).ready(function () {
    $("#dropDownSearchSick").select2({width: '100%'}
    );
});

$(document).ready(function () {
    $("#dropDownSearchSrg").select2({width: '100%'}
    );
});

$(document).ready(function () {
    $("#dropDownSearchFood").select2({width: '100%'}
    );
});

$(document).ready(function () {
    $("#dropDownSearchAllergenicity").select2({width: '100%'}
    );
});

$(document).ready(function () {
    $("#dropDownSearchSampleName").select2({width: '100%'}
    );
});

$(document).ready(function () {
    $("#dropDownSearchChromosome").select2({width: '100%'}
    );
});


//--------------------------------------

$(document).ready(function () {
    $("#dropDownSearch").select2({
        width: '100%'
    });
});

$(document).ready(function () {
    $("#dropDownSearchs1").select2({
        width: '100%'
    });
    $("#dropDownSearchs2").select2({
        width: '100%'
    });
    $("#dropDownSearchs3").select2({
        width: '100%'
    });
    $("#dropDownSearchs4").select2({
        width: '100%'
    });
    $("#dropDownSearchs5").select2({
        width: '100%'
    });
    $("#dropDownSearchs6").select2({
        width: '100%'
    });
    $("#dropDownSearchs7").select2({
        width: '100%'
    });
    $("#dropDownSearchs8").select2({
        width: '100%'
    });
    $("#dropDownSearchs9").select2({
        width: '100%'
    });
    $("#dropDownSearchs10").select2({
        width: '100%'
    });
    $("#dropDownSearchs11").select2({
        width: '100%'
    });
    $("#dropDownSearchs12").select2({
        width: '100%'
    });
});


$(":file").filestyle(
    {
        buttonBefore: true,
        buttonText: 'Pasirinkite failą'
    }
);

//----------------------------------
/*
$(document).ready(function () {
  $("#e7").select2({
    data: _.map(_.range(1,201), function(i) { 
      return {id:i, text: 'number ' + i}; }),
    query: function (q) {
      var pageSize = 20, results = _.filter(this.data, function(e) {
        return (q.term == "" ||
           e.text.toUpperCase().indexOf(
               q.term.toUpperCase()) >= 0 );
      });
      q.callback({results:results.slice((q.page-1)*pageSize, q.page*pageSize),
                  more:results.length >= q.page*pageSize });
    }
  });
})
*/


