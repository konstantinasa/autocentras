<?php

$lang['upload_userfile_not_set'] = "Nepavyko rasti siuntimo kintamojo pavadinto userfile.";
$lang['upload_file_exceeds_limit'] = "Įkelta byla viršija didžiausią leidžiamą dydi nustatytą PHP konfiguracinėje byloje.";
$lang['upload_file_exceeds_form_limit'] = "Įkelta byla viršija didžiausią nustatytą dydį pagal siuntimo formą.";
$lang['upload_file_partial'] = "Byla buvo tik dalinai nusiųsta.";
$lang['upload_no_temp_directory'] = "Dingo laikinasis aplankas.";
$lang['upload_unable_to_write_file'] = "Nepavyko bylos įrašyti į diską.";
$lang['upload_stopped_by_extension'] = "Bylos įkėlimas buvo sustabdytas išplėtimo.";
$lang['upload_no_file_selected'] = "Nepasirinkote bylos išsiuntimui.";
$lang['upload_invalid_filetype'] = "Bylos tipas kurį bandote išsiųsti yra negalimas.";
$lang['upload_invalid_filesize'] = "Byla kurią bandote išsiųsti yra didesnė nei leidžiamas dydis.";
$lang['upload_invalid_dimensions'] = "Paveikslėlis kurį bandote išsiųsti viršija didžiausią leistiną aukštį arba plotį.";
$lang['upload_destination_error'] = "Kilo problema perkeliant išsiųstą bylą į galitinį tikslą.";
$lang['upload_no_filepath'] = "įkėlimo kelias, panašu, nėra teisingas.";
$lang['upload_no_file_types'] = "Jūs nenurodėt jokių leistinų bylų tipų.";
$lang['upload_bad_filename'] = "Bylos tipas kurį pateikėte jau egzistuoja serveryje.";
$lang['upload_not_writable'] = "Siuntimo aplankas, panašu, nėra rašomas.";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */
