<?php

$lang['ftp_no_connection']			= "Nepavyko nustatyti teisingo prisijungimo ID. Prašome įsitikinti, kad esate prisijungę prieš atliekant pakeitimus su bylomis.";
$lang['ftp_unable_to_connect']		= "Nepavyko prisijungti prie jūsų FTP serverio naudojant nurodytą hostname.";
$lang['ftp_unable_to_login']		= "Nepavyko prisiregistruoti prie jūsų FTP serverio. Prašome patikrinti vartotojo vardą ir slaptažodį.";
$lang['ftp_unable_to_makdir']		= "Nepavyko sukurti nurodytos direktorijos.";
$lang['ftp_unable_to_changedir']	= "Nepavyko nueiti į nurodytą direktoriją.";
$lang['ftp_unable_to_chmod']		= "Nepavyko pakeisti bylų teisių. Prašome patikrinti kelią.";
$lang['ftp_unable_to_upload']		= "Nepavyko įkelti nurodytos bylos. Prašome patikrinti kelią.";
$lang['ftp_unable_to_download']		= "Nepavyko atsiųsti nurodytos bylos. Prašome patikrinti kelią.";
$lang['ftp_no_source_file']			= "Nepavyko rasti šaltinio. Prašome patikrinti kelią.";
$lang['ftp_unable_to_rename']		= "Nepavyko pervadinti bylos.";
$lang['ftp_unable_to_delete']		= "Nepavyko ištrynti bylos.";
$lang['ftp_unable_to_move']			= "Nepavyko perkelti failo. Prašome įsitikinti kad tokia direktorija egzistuoja.";


/* End of file ftp_lang.php */
/* Location: ./system/language/english/ftp_lang.php */
