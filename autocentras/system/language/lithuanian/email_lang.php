<?php

$lang['email_must_be_array'] = "Elektroninio pašto patvirtinimo metodas turi gauti masyvą.";
$lang['email_invalid_address'] = "Klaidingas elektroninio pašto adresas: %s";
$lang['email_attachment_missing'] = "Nepavyko nustatyti elektroninio pašto priedo: %s";
$lang['email_attachment_unreadable'] = "Nepavyko atverti šio priedo: %s";
$lang['email_no_recipients'] = "Turite nurodyti gavėjus: To, Cc, arba Bcc";
$lang['email_send_failure_phpmail'] = "Nepavyko išsiųsti elektroninio pašto naudojant PHP mail(). Gali būti, kad jūsų serveris nėra sukonfiguruotas siūsti pašto šiuo metodu.";
$lang['email_send_failure_sendmail'] = "Nepavyko išsiųsti elektroninio pašto naudojant PHP Sendmail. Gali būti, kad jūsų serveris nėra sukonfiguruotas siūsti pašto šiuo metodu.";
$lang['email_send_failure_smtp'] = "Nepavyko išsiųsti elektroninio pašto naudojant PHP SMTP. Gali būti, kad jūsų serveris nėra sukonfiguruotas siūsti pašto šiuo metodu.";
$lang['email_sent'] = "Jūsų žinutė buvo sėkmingai nusiųsta naudojant šį protokolą: %s";
$lang['email_no_socket'] = "Nepavyko atidaryti lizdo į Sendmail. Prašome patikrinti nustatymus.";
$lang['email_no_hostname'] = "Jūs nenurodėte SMTP hostname.";
$lang['email_smtp_error'] = "Susidurta su šia SMTP klaida: %s";
$lang['email_no_smtp_unpw'] = "Klaida: turite įvesti SMTP vartotojo vardą ir slaptažodį.";
$lang['email_failed_smtp_login'] = "Nepavyko išsiųsti AUTH LOGIN komandos. Klaida: %s";
$lang['email_smtp_auth_un'] = "Nepavyko autentifikuoti vartotojo vardo. Klaida: %s";
$lang['email_smtp_auth_pw'] = "Nepavyko autentifikuoti slaptažodžio. Klaida: %s";
$lang['email_smtp_data_failure'] = "Nepavyko išsiųsti duomenų: %s";
$lang['email_exit_status'] = "Išėjimo kodas: %s";


/* End of file email_lang.php */
/* Location: ./system/language/english/email_lang.php */
