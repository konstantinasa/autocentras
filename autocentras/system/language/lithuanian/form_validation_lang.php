<?php

$lang['required']			= "<div class='alert alert-danger'>Laukelis %s yra privalomas.</div>";
$lang['isset']				= "<div class='alert alert-danger'>Laukelis %s turi turėti reikšmę.</div>";
$lang['valid_email']		= "<div class='alert alert-danger'>Laukelis %s turi turėti teisingą elektroninio pašto adresą.</div>";
$lang['valid_emails']		= "<div class='alert alert-danger'>Laukelis %s turi turėti tik teisingus elektroninio pašto adresus.</div>";
$lang['valid_url']			= "<div class='alert alert-danger'>Laukelis %s turi turėti teisingą URL.</div>";
$lang['valid_ip']			= "<div class='alert alert-danger'>Laukelis %s turi turėti teisingą IP.</div>";
$lang['min_length']			= "<div class='alert alert-danger'>Laukelis %s turi būti bent %s simbolių ilgio.</div>";
$lang['max_length']			= "<div class='alert alert-danger'>Laukelis %s negali viršyti %s simbolių ilgio.</div>";
$lang['exact_length']		= "<div class='alert alert-danger'>Laukelis %s turi turėti %s simbolius.</div>";
$lang['alpha']				= "<div class='alert alert-danger'>Laukelis %s gali būti sudarytas tik iš alfabetinių simbolių.</div>";
$lang['alpha_numeric']		= "<div class='alert alert-danger'>Laukelis %s gali būti sudarytas tik iš alfabetinių bei skaitinių simbolių.</div>";
$lang['alpha_dash']			= "<div class='alert alert-danger'>Laukelis %s gali būti sudarytas tik iš alfabetinių, skaitinių simbolių bei apatinių ir paprastų brūkšnių.</div>";
$lang['numeric']			= "<div class='alert alert-danger'>Laukelis %s gali būti sudarytas tik iš skaičių.</div>";
$lang['is_numeric']			= "<div class='alert alert-danger'>Laukelis %s gali būti sudarytas tik iš skaitinių simbolių.</div>";
$lang['integer']			= "<div class='alert alert-danger'>Laukelis %s privalo turėti sveikajį skaičių.</div>";
$lang['regex_match']		= "<div class='alert alert-danger'>Laukelis %s yra neteisingo formato.</div>";
$lang['matches']			= "<div class='alert alert-danger'>Laukelis %s neatitinka laukelio %s.</div>";
$lang['is_unique'] 			= "<div class='alert alert-danger'>Laukelis %s privalo turėti unikalią reikšmę.</div>";
$lang['is_natural']			= "<div class='alert alert-danger'>Laukelis %s privalo turėti naturalų skaičių.</div>";
$lang['is_natural_no_zero']	= "<div class='alert alert-danger'>Laukelis %s privalo turėti skaičių didesnį už nulį.</div>";
$lang['decimal']			= "<div class='alert alert-danger'>Laukelis %s privalo turėti dešimtainį skaičių.</div>";
$lang['less_than']			= "<div class='alert alert-danger'>Laukelis %s privalo turėti skaičių mažesnį už %s.</div>";
$lang['greater_than']		= "<div class='alert alert-danger'>Laukelis %s privalo turėti skaičių didesnį už %s.</div>";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
