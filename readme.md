Automobiliu centras / Car repair service web app

Sistemos idiegimo instrukcija Windows aplinkoje

1.	Atsisiuskite ir isidiekite WAPP iranki (https://bitnami.com/stack/wapp/installer)
2.	I WAPP irankio htdocs aplanka perkelkite svetaines aplanka su failais, kuris randasi suspaustame .zip formato faile.
htdocs aplanka pagal nutylejima galima rasti C:\wapp\apache2\htdocs
Sveitaines aplanko pavadinimas yra autocentras

3.	Ueikite i WAPP aplikacija, tai galite padaryti Windows paiekoj parae WAPP. 
4.	Atsidariusiame lange paspauskite Go to Application
5.	Atsidariusioje narykleje paspauskite paskutini meniu punkta Go to phpPgAdmin.
6.	Prisijunkite prie duomenu bazes. Vartotojo vardas  postgres, slaptaodio nera.
7.	Sukurkite nauja duomenu baze ir iveskite sekancius duomenys.
	a.	Paspauskite mygtuka Create database
	b.	Name  auto_centras
	c.	Template  template0
	d.	Encoding  UTF8
	e.	Collation - English_United Kingdom.1252
	f.	Character Type - English_United Kingdom.1252
8.	Ueikite ant naujai sukurtos duomenu bazes (meniu i kaires)
	a.	Virutiniame meniu spauskite  SQL
	b.	Apacioje spauskite Choose File
	c.	Pasirinkite db_auto_final.sql faila i .zip archyvo
	d.	Spauskite Execute
	e.	Turetumete gauti praneima apie sekminga duomenu bazes sukurima
9.	Pasikeiskite duomenu bazes slaptaodi. Kai pasikeisite ji turite irayti i sveitaines database.php faila, kuris randasi C:\wapp\apache2\htdocs\autocentras\application\config\database.php
Suraskite iraa: 
"$db['default']['password'] = '12345'; 
iame irae slaptaodis yra 12345, taip yra nustatyta ir svetaines faile, jeigu yra poreikis  pasikeiskite. 
10.	Ueikite narykleje i: http://localhost/autocentras
Prisijungimo duomenys: 
Vardas: admin@admin.com
Slaptaodis: password