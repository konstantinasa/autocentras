--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE groups (
    id integer NOT NULL,
    name character varying(20) NOT NULL,
    description character varying(100) NOT NULL,
    CONSTRAINT check_id CHECK ((id >= 0))
);


ALTER TABLE groups OWNER TO postgres;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE groups_id_seq OWNER TO postgres;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: login_attempts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE login_attempts (
    id integer NOT NULL,
    ip_address character varying(15),
    login character varying(100) NOT NULL,
    "time" integer,
    CONSTRAINT check_id CHECK ((id >= 0))
);


ALTER TABLE login_attempts OWNER TO postgres;

--
-- Name: login_attempts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE login_attempts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE login_attempts_id_seq OWNER TO postgres;

--
-- Name: login_attempts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE login_attempts_id_seq OWNED BY login_attempts.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    ip_address character varying(45),
    username character varying(100),
    password character varying(255) NOT NULL,
    salt character varying(255),
    email character varying(100) NOT NULL,
    activation_code character varying(40),
    forgotten_password_code character varying(40),
    forgotten_password_time integer,
    remember_code character varying(40),
    created_on integer NOT NULL,
    last_login integer,
    active integer,
    first_name character varying(50),
    last_name character varying(50),
    company character varying(100),
    phone character varying(20),
    CONSTRAINT check_active CHECK ((active >= 0)),
    CONSTRAINT check_id CHECK ((id >= 0))
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE messages (
    message text,
    fl_name character varying,
    email character varying,
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    date_time timestamp without time zone
);


ALTER TABLE messages OWNER TO postgres;

--
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE orders (
    service_descr_cust text,
    atvykimo_data date,
    automobilis character varying(500),
    marke character varying(500),
    valst_nr character varying(500),
    draudimas character varying(500),
    vardas character varying(500) NOT NULL,
    pavarde character varying(500),
    tel_nr character varying,
    el_pastas character varying(500) NOT NULL,
    service bigint,
    atvykimo_laikas integer,
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    production_date date,
    kiekis smallint
);


ALTER TABLE orders OWNER TO postgres;

--
-- Name: services; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE services (
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    service character varying(500),
    description character varying(1000),
    price_from numeric(5,2),
    price_to numeric(5,2)
);


ALTER TABLE services OWNER TO postgres;

--
-- Name: times; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE times (
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    laikas character varying(50)
);


ALTER TABLE times OWNER TO postgres;

--
-- Name: users_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL,
    CONSTRAINT users_groups_check_group_id CHECK ((group_id >= 0)),
    CONSTRAINT users_groups_check_id CHECK ((id >= 0)),
    CONSTRAINT users_groups_check_user_id CHECK ((user_id >= 0))
);


ALTER TABLE users_groups OWNER TO postgres;

--
-- Name: users_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_groups_id_seq OWNER TO postgres;

--
-- Name: users_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_groups_id_seq OWNED BY users_groups.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY login_attempts ALTER COLUMN id SET DEFAULT nextval('login_attempts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_groups ALTER COLUMN id SET DEFAULT nextval('users_groups_id_seq'::regclass);


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO groups VALUES (1, 'admin', 'Administrator');
INSERT INTO groups VALUES (2, 'user', 'General User');


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('groups_id_seq', 1, false);


--
-- Data for Name: login_attempts; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: login_attempts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('login_attempts_id_seq', 1, false);


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO messages VALUES ('dasdasd;;;:;;34!@#$%^&*', 'Kostas Are', 'arekon9@gmail.com', 89, '2016-05-25 23:06:53.065');
INSERT INTO messages VALUES ('dasdasda', 'Kostas Are', 'arekon9@gmail.com', 91, '2016-05-25 23:15:11.162');
INSERT INTO messages VALUES ('dasdasd', 'Kostas Are', 'arekon9@gmail.com', 92, '2016-05-25 23:15:38.488');
INSERT INTO messages VALUES ('dasds', 'Kostas Are', 'arekon9@gmail.com', 95, '2016-05-25 23:20:31.593');
INSERT INTO messages VALUES ('dasdasdasdasd', 'Kostas Are', 'arekon9@gmail.com', 97, '2016-05-25 23:21:41.819');
INSERT INTO messages VALUES ('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Vardenis PAvardenis', 'vard@pavard.com', 98, '2016-05-26 01:09:13.746');
INSERT INTO messages VALUES ('klasuimas', 'vardas pavarde', 'pastasfass@lt.lt', 99, '2016-05-26 11:17:53.438');


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO orders VALUES ('Kostas', '2016-04-15', '!@#$%^&*()_++_)(*,,,&^%$#@!~~~[][]{}{}{<><>???-*//*-*+,,,++.....+=====&&&781234567890,,', '!@#$%^&*()_++_)(*,,,&^%$#@!~~~[][]{}{}{<><>???-*//*-*+,,,++.....+=====&&&781234567890,,', '!@#$%^&*()_++_)(*,,,&^%$#@!~~~[][]{}{}{<><>???-*//*-*+,,,++.....+=====&&&781234567890,,', '!@#$%^&*()_++_)(*,,,&^%$#@!~~~[][]{}{}{<><>???-*//*-*+,,,++.....+=====&&&781234567890,,', '!@#$%^&*()_++_)(*,,,&^%$#@!~~~[][]{}{}{<><>???-*//*-*+,,,++.....+=====&&&781234567890,,', '!@#$%^&*()_++_)(*,,,&^%$#@!~~~[][]{}{}{<><>???-*//*-*+,,,++.....+=====&&&781234567890,,', '!@#$%^&*()_++_)(*,,,&^%$#@!~~~[][]{}{}{<><>???-*//*-*+,,,++.....+=====&&&781234567890,,', 'abc5@abc.lt', 16, 8, 66, NULL, NULL);
INSERT INTO orders VALUES ('', '2016-04-13', '', '', '', '', '', '', '', '', 16, 1, 71, NULL, NULL);
INSERT INTO orders VALUES ('keicam ratus', '2016-04-14', 'BMW', 'V50', 'KKD 789', 'Nera', 'Karolis', 'Pomka', '+37045684856', 'abc5@abc.lt', 15, 1, 42, NULL, NULL);
INSERT INTO orders VALUES ('', '2016-04-19', '', '', '', '', '', '', '', '', 16, 1, 73, NULL, 3);
INSERT INTO orders VALUES ('', '2016-04-02', '', '', '', '', '', '', '', '', 17, 1, 74, NULL, 6);
INSERT INTO orders VALUES ('', '2016-04-08', '', '', '', '', '', '', '', '', 14, 1, 75, NULL, 1);
INSERT INTO orders VALUES ('', '2016-04-27', '', '', '', '', '', '', '', '', 18, 1, 76, NULL, 3);
INSERT INTO orders VALUES ('', '2016-04-15', '', '', '', '', '', '', '', '', 16, 1, 72, NULL, NULL);
INSERT INTO orders VALUES ('jhiojij', '2016-04-13', '', '', '', '', '', '', '', '', 14, 1, 70, NULL, NULL);
INSERT INTO orders VALUES ('', '2016-04-13', '', '', '', '', '', '', '', '', 17, 2, 77, NULL, 1);
INSERT INTO orders VALUES ('', '2016-05-05', '', '', '', '', 'Karolis', '', '', '', 16, 8, 78, NULL, 1);
INSERT INTO orders VALUES ('KAzkas blogai aniaaa2asdasd', '2016-04-16', 'BMW', 'E3', 'KKD 788', 'Gjensidige9', 'Karolis9', 'bbb2', '+370456848567', 'abc5@abc2.lt', 16, 7, 19, NULL, NULL);


--
-- Data for Name: services; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO services VALUES (17, 'Pakabos tvarkymas', NULL, 50.00, 300.00);
INSERT INTO services VALUES (15, 'Ratų suvedimas', ' ', 19.00, 20.89);
INSERT INTO services VALUES (18, 'Variklio remontas', 'Vidaus degimo ir elektriniu varikliu remontas', 100.00, 500.00);
INSERT INTO services VALUES (16, 'Diagnostika', 'Visa automobilio diagnostika', 10.00, 50.00);
INSERT INTO services VALUES (0, '-', ' Pirmas paslaugos įrašas', 0.10, NULL);
INSERT INTO services VALUES (100, 'Elektronikos montavimas', ' ', 20.00, NULL);
INSERT INTO services VALUES (102, 'Sedynių tvarkymas', ' ', 5.00, NULL);
INSERT INTO services VALUES (103, 'Stogo remontas', 'Stogo remontas profesionaliais įrankiais', 80.00, 100.00);
INSERT INTO services VALUES (14, 'Ratų montavimas', 'Ratų pakeitimas', 12.00, 23.00);


--
-- Data for Name: times; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO times VALUES (1, '10');
INSERT INTO times VALUES (2, '11');
INSERT INTO times VALUES (5, '12');
INSERT INTO times VALUES (6, '13');
INSERT INTO times VALUES (7, '14');
INSERT INTO times VALUES (8, '15');
INSERT INTO times VALUES (9, '16');
INSERT INTO times VALUES (10, '17');
INSERT INTO times VALUES (11, '18');
INSERT INTO times VALUES (12, '19');
INSERT INTO times VALUES (13, '20');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users VALUES (79, '::1', 'kestas123.pavard', '$2y$08$oyVhT0eRCNTGk7e7ufaSn.T/kLS8KJOKVtfyha37GJ4SWXKcDCERy', NULL, 'kestas123@gmail.com', NULL, NULL, NULL, NULL, 1462435976, 1462435987, 1, 'kestas123', 'pavard', NULL, NULL);
INSERT INTO users VALUES (4, NULL, 'user', '$2y$08$kq3iJ3SG28M054lqhJYGteb5OCZl4c5f9JolpWUnKr5r7pQU9MVdi', NULL, 'user@user.com', NULL, NULL, NULL, NULL, 123123, 1464605752, 1, 'user', 'user', NULL, NULL);
INSERT INTO users VALUES (1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1464605973, 1, 'Admin', 'istrator', 'ADMIN', '0');
INSERT INTO users VALUES (67, '::1', 'vardenis.pavardenis', '$2y$08$Os3MKSjBCUX6DG7eUDubguwD6K1CHe9rF2.irpQY9snc1Ppj6Cd7u', NULL, 'vard.pavard@gmail.com', NULL, NULL, NULL, NULL, 1461782416, 1461782782, 1, 'vardenis', 'pavardenis', NULL, NULL);
INSERT INTO users VALUES (68, '::1', 'tomas.tomas', '$2y$08$VllzigdO4u71fweRxKepxuDZzyrIqxX0UGucPQt39qPkF9/rkV4ve', NULL, 'tom@tom.lt', NULL, NULL, NULL, NULL, 1461784690, 1461784727, 1, 'tomas', 'tomas', NULL, NULL);


--
-- Data for Name: users_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users_groups VALUES (7, 1, 1);
INSERT INTO users_groups VALUES (8, 67, 1);
INSERT INTO users_groups VALUES (9, 68, 2);
INSERT INTO users_groups VALUES (12, 79, 1);
INSERT INTO users_groups VALUES (13, 4, 2);


--
-- Name: users_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_groups_id_seq', 13, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 103, true);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: login_attempts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY login_attempts
    ADD CONSTRAINT login_attempts_pkey PRIMARY KEY (id);


--
-- Name: messages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: services_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: time_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY times
    ADD CONSTRAINT time_pkey PRIMARY KEY (id);


--
-- Name: uc_users_groups; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_groups
    ADD CONSTRAINT uc_users_groups UNIQUE (user_id, group_id);


--
-- Name: users_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_groups
    ADD CONSTRAINT users_groups_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: services_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT services_fkey FOREIGN KEY (service) REFERENCES services(id);


--
-- Name: time_feky; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT time_feky FOREIGN KEY (atvykimo_laikas) REFERENCES times(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

